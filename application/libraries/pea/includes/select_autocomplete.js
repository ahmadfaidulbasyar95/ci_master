(function() {
	window.addEventListener('load', function() { 
		$('.select_autocomplete').each(function(index, el) {
			var elem = $(this);
			if (!elem.data('select_autocomplete_load')) {
				elem.data('select_autocomplete_load', 1).hide();
				var value      = elem.data('value');
				var multiple   = (elem.attr('multiple') != undefined) ? 1 : 0;
				var required   = (elem.attr('required') != undefined) ? 'required="required"' : '';
				
				$('<input type="text" class="form-control select_autocomplete_input" value="" '+required+' title="'+elem.attr('title')+'" placeholder="'+elem.attr('title')+'"><div class="select_autocomplete_result list-group" style="display: none;position: absolute;box-shadow: rgba(0, 0, 0, 0.53) 0px 5px 5px;max-height: 50vh; overflow:auto;"></div>').insertAfter(elem);
				
				var input  = elem.next('.select_autocomplete_input');
				var result = input.next('.select_autocomplete_result');

				input.on('keyup', delay(function(event) {
					var s = input.val().toLowerCase(); 
					if (s || value) {
						var option_html = '';
						elem.children('option').filter(function() {
							var o  = $(this); 
							var ok = o.html();
							var ov = o.attr('value');
							var r  = (ok.toLowerCase().search(s) != -1 && ov) ? true : false;
							if (r) {
								if (multiple) {
									if ($.inArray(ov, value) != -1) {
										var os = 1;
									}else{
										var os = 0;
									}
								}else{
									if (value == ov) {
										var os = 1;
									}else{
										var os = 0;
									}
								}
								option_html += '<a href="#" class="list-group-item '+(os ? 'active' : '')+'" data-value="'+ov+'">'+ok+'</a>';
							}
						});
						result.html(option_html);
						if (s) {
							result.show();
						}else{
							input.trigger('focusout');
						}
					}else{
						elem.val('').trigger('change');
						result.html('');
					}
				}, 1000)).trigger('keyup');
			}
		});
		$('body').on('click', '.select_autocomplete_result a', function(event) {
			event.preventDefault();
			var item     = $(this);
			var result   = item.parent('.select_autocomplete_result'); 
			var input    = result.prev('.select_autocomplete_input');
			var elem     = input.prev('.select_autocomplete');
			var multiple = (elem.attr('multiple') != undefined) ? 1 : 0;
			if (multiple) {
				// multiselect
			}else{
				result.children('.active').removeClass('active');
				item.addClass('active');
				elem.val(item.data('value')).trigger('change');
			}
		});
		$('body').on('focusin', '.select_autocomplete_input', function(event) {
			$(this).next('.select_autocomplete_result').show();
		});
		$('body').on('focusout', '.select_autocomplete_input', function(event) {
			var input = $(this);
			setTimeout(function() {
				var result   = input.next('.select_autocomplete_result');
				var elem     = input.prev('.select_autocomplete');
				var multiple = (elem.attr('multiple') != undefined) ? 1 : 0;
				var item     = result.children('.active');
				result.hide();
				if (multiple) {
					// multiselect
				}else{
					input.val(item.html());
				}
			}, 400);
		});
	}, false);
})();