(function() {
	window.addEventListener('load', function() { 
		$('.fm_textdependent').each(function(index, el) {
			var elem = $(this);
			if (!elem.data('fm_textdependent_load')) {
				elem.data('fm_textdependent_load', 1);
				var token   = elem.data('token');
				var cache   = elem.data('cache');
				var val_def = elem.val();
				elem.on('keyup', function(event) {
					val_def = elem.val();
				});
				$('[name="'+elem.data('dependent')+'"]').on('change', function(event) {
					var v = $(this).val();
					if (v) {
						$.ajax({
							url: _URL+'_T/getdata',
							type: 'POST',
							dataType: 'json',
							data: {token: token, cache: cache, key: v},
						})
						.done(function(out) {
							if (out[0] != undefined) {
								elem.val(out[0].value).attr('readonly', 'readonly');
							}else{
								elem.val(val_def).removeAttr('readonly');
							}
						})
						.fail(function() {
							alert("Something Wrong. Please try again later or reload this page !");
						});
					}else{
						elem.val(val_def).removeAttr('readonly');
					}
				});
			}
		});
	}, false);
})();