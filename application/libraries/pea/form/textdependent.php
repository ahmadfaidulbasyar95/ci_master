<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once __DIR__.'/text.php';
class lib_pea_frm_textdependent extends lib_pea_frm_text
{	
	public $referenceTable      = '';
	public $referenceFieldValue = '';
	public $referenceCache      = '';
	public $referenceCondition  = '';
	public $token_load          = 0;
	public $dependent           = array();

	function __construct($opt, $name)
	{
		parent::__construct($opt, $name);
	}

	public function setReferenceTable($referenceTable = '')
	{
		if ($referenceTable) $this->referenceTable = $referenceTable;
	}

	public function setReferenceField($value = '')
	{
		if ($value) $this->referenceFieldValue = $value;
	}

	public function setReferenceCondition($referenceCondition = '')
	{
		if ($referenceCondition) $this->referenceCondition = 'AND '.$referenceCondition;
	}

	public function setReferenceCache($referenceCache = 'files/cache/')
	{
		$this->referenceCache = $referenceCache;
	}

	public function setDependent($name = '', $field = '')
	{
		if ($name and $field) {
			$this->dependent = array(
				'name'  => $name,
				'field' => $field,
			);
			$this->setIncludes('textdependent','js');
		}else{
			$this->dependent = array();
		}
	}

	public function getToken()
	{
		if (!$this->token_load) {
			$this->token_load = 1;
			if (!$this->isPlainText and $this->dependent) {
				$token = 'SELECT '.$this->dependent['field'].' AS `key`, '.$this->referenceFieldValue.' AS `value` FROM '.$this->referenceTable.' WHERE '.$this->referenceFieldValue.' != "" AND '.$this->dependent['field'].' != "" '.$this->referenceCondition;
				$this->db->load->model('_encrypt_model');
				$this->addAttr('data-token="'.$this->db->_encrypt_model->encodeToken($token, 60).'"');
				$this->addAttr('data-dependent="'.$this->dependent['name'].'"');
				$this->addClass('fm_textdependent');
				if ($this->referenceCache) {
					$this->addAttr('data-cache="'.$this->db->_encrypt_model->encodeToken($this->referenceCache, 60).'"');
				}
			}
		}
	}

	public function getForm($index = '', $values = array())
	{
		$this->getToken();
		return parent::getForm($index, $values);
	}

}