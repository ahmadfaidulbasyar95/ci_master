<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once __DIR__.'/text.php';
class lib_pea_frm_copy extends lib_pea_frm_text
{	
	public $copyMsg = 'Text Copied';

	function __construct($opt, $name)
	{
		parent::__construct($opt, $name);
		$this->setPlainText();
		$this->setIncludes('copy.min', 'js');
	}

	public function setCopyMsg($msg='')
	{
		$this->copyMsg = $msg;
	}

	public function getForm($index = '', $values = array())
	{
		$form = '';
		if ($this->init == 'roll' and !$this->isMultiinput) $form .= '<td>';
		$form .= $this->formBefore;
		if ($this->init != 'roll') $form .= '<div class="form-group">';
		if (!$this->isMultiform and !$this->isMultiinput and in_array($this->init, ['edit','add'])) $form .= '<label>'.$this->title.'</label>';

		$id    = $this->table.$this->init.$this->name.$index;
		$value = ($this->displayFunction) ? call_user_func($this->displayFunction, $this->getValue($index), $this->getValueID($index), $index, $values) : $this->getValue($index);
		$form .= '<div class="fm_copy" style="border: 1px solid #0000004d;padding: 10px;"><a href="#copy" style="float: right;" onclick=\'copyToClipboard("'.$id.'");alert("'.$this->copyMsg.'")\'><i class="fa fa-copy"></i></a><div id="'.$id.'" style="margin-right: 20px;">'.$value.'</div></div>';
		
		if ($this->tips) $form .= '<div class="help-block">'.$this->tips.'</div>';
		if ($this->init != 'roll') $form .= '</div>';
		$form .= $this->formAfter;
		if ($this->init == 'roll' and !$this->isMultiinput) $form .= '</td>';
		return $form;
	}
}