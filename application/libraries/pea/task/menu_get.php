<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!empty($_GET['v'])) {
	$this->load->model('_db_model');
	$dt = $this->_db_model->cache('getRow','SELECT `id`,`position_id`,`title`,`url`,`protect` FROM `menu` WHERE `uri`="'.addslashes($_GET['v']).'" AND `active`=1','files/cache_sys/menu/');
	include_once __DIR__.'/../../output.php';
	lib_output_json($dt);
}