<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!empty($_POST['token'])) {
	$this->load->model('_encrypt_model');
	$token = $this->_encrypt_model->decodeToken($_POST['token']);
	if ($token) {
		unset($_POST['token']);
		$cache = '';
		if (isset($_POST['cache'])) {
			$cache = $this->_encrypt_model->decodeToken($_POST['cache']);
			unset($_POST['cache']);
		}
		if (isset($_POST['key'])) {
			$find_key = $_POST['key'];
			unset($_POST['key']);
		}
		foreach ($_POST as $key => $value) {
			if (is_array($value)) {
				foreach ($value as $key1 => $value1) {
					$value[$key1] = addslashes($value1);
				}
				$value = '"'.implode('","', $value).'"';
			}else{
				$value = addslashes($value);
			}
			$token = str_replace('['.$key.']', $value, $token);
		}
		$this->load->model('_db_model');
		include_once __DIR__.'/../../output.php';
		if (isset($find_key)) {
			if ($cache) {
				$data = $this->_db_model->cache('getAssoc',$token, $cache);
			}else{
				$data = $this->_db_model->getAssoc($token);
			}
			if (isset($data[$find_key])) {
				lib_output_json([$data[$find_key]]);
			}else{
				lib_output_json([]);
			}
		}else{
			if ($cache) {
				$data = $this->_db_model->cache('getAll',$token, $cache);
			}else{
				$data = $this->_db_model->getAll($token);
			}
			lib_output_json($data);
		}
	}
}