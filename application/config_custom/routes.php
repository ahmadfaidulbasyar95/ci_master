<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['([a-z0-9-]+).html'] = function($value='') {
	$value = file_get_contents($this->config->config['base_url'].'_T/menu_get?v='.urlencode($value));
	$value = @(array)json_decode($value, 1);

	if ($value) {
		$GLOBALS['_MENU_CURRENT'] = $value;

		$parse_url = parse_url($value['url']);
		parse_str(@$parse_url['query'], $parse_url['query']);

		foreach ($parse_url['query'] as $key1 => $value1) {
			if (!isset($_GET[$key1])) {
				$_GET[$key1] = $value1;
			}
		}
		return preg_replace('~\/$~','',$parse_url['path']);
	}else{
		header('location:'.$this->config->config['base_url']);
	}
};

$route[$_SESSION['cache_cfg']['dashboard']['login_uri']] = 'admin/user/act/login';

if ($_SESSION['cache_cfg']['site_url'] == $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']) {
	$parse_url = parse_url($_SESSION['cache_cfg']['site']['home_uri']);
	parse_str(@$parse_url['query'], $parse_url['query']);
	foreach ($parse_url['query'] as $key => $value) {
		if (!isset($_GET[$key])) {
			$_GET[$key] = $value;
		}
	}
	$route['default_controller'] = preg_replace('~\/$~','',$parse_url['path']);
}