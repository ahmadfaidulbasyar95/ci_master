<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('UTC');
session_start();
if (!isset($_SESSION['cache_cfg']['site'])) {
	$_SESSION['cache_cfg'] = [
		'site'      => [],
		'dashboard' => [],
		'user'      => [],
	];
	$p = FCPATH.'files/cache_sys/config/';
	foreach ($_SESSION['cache_cfg'] as $key => $value) {
		if (is_file($p.$key.'.cfg')) {
			$_SESSION['cache_cfg'][$key] = @(array)json_decode(file_get_contents($p.$key.'.cfg'), 1);
		}
	}
	$_SESSION['cache_cfg']['site_url'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SESSION['cache_cfg']['site']['domain'].str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
}

$config['base_url'] = $_SESSION['cache_cfg']['site_url'];

define('_SALT', $config['encryption_key']);