<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		if (!in_array($this->_tpl_model->method, ['login','logout','register','forget','detail'])) {
			$this->_tpl_model->user_login_validate();
		}elseif (!in_array($this->_tpl_model->method, ['logout','detail']) and !empty($_SESSION['user_login'][0])) {
			$this->_tpl_model->user_login_validate();
		}
	}

	function profile()
	{
		$this->register();
	}

	function register()
	{
		$id   = @intval($this->_tpl_model->user['id']);
		$form = $this->_pea_model->newForm('user');

		$form->initEdit(!empty($id) ? 'WHERE `id`='.$id : '');

		if (!$id) {
			$group_id = @intval($_GET['id']);
			if (!$group_id) {
				$group_id = intval($this->_tpl_model->config('user', 'group_id'));
			}
			$group_data = $this->_tpl_model->_db_model->getRow('SELECT `title`,`approval` FROM `user_group` WHERE `id`='.$group_id);
			if ($group_data) {
				$group_title = $group_data['title'];
				$form->edit->setData('group_id', $group_id);
				switch ($group_data['approval']) {
					case '1':
						$form->edit->addExtraField('active', 1);
						break;

					case '2':
						$form->edit->addExtraField('active', 0);
						$form->edit->setSuccessMsg('Pendaftaran '.$group_title.' Berhasil');
						break;
					
					default:
						echo $this->_tpl_model->msg('Pendaftaran '.$group_title.' Ditutup', 'danger');
						$this->_tpl_model->show();
						return false;
						break;
				}
			}else{
				show_error('Invalid Group');
			}
		}
		
		$form->edit->setHeader(!empty($id) ? 'Profil' : 'Pendaftaran '.$group_title);
		
		$form->edit->addInput('name','text');
		$form->edit->input->name->setTitle('Nama');
		$form->edit->input->name->setRequire();

		if (!$id) {					
			$form->edit->addInput('username','text');
			$form->edit->input->username->setTitle('Username');
			$form->edit->input->username->setRequire();
			$form->edit->input->username->setUniq();
		
			$form->edit->addInput('password','text');
			$form->edit->input->password->setTitle('Password');
			$form->edit->input->password->setType('password');
			$form->edit->input->password->setRequire();
			$name = $form->edit->input->password->getName();
			if (!empty($_POST[$name])) {
				if ($_POST[$name] != @$_POST['password_re']) {
					$form->edit->input->password->msg = str_replace('{msg}', '<b>Password dan Re-Password</b> tidak sama', $form->edit->input->password->failMsgTpl);
				}else{
					$this->load->model('_encrypt_model');
					$_POST['pwd'] = $_POST[$name];
					$_POST[$name] = $this->_encrypt_model->encode($_POST[$name]);
				}
			}

			$form->edit->addInput('password_re', 'plaintext');
			$form->edit->input->password_re->setTitle('Re-Password');
			$form->edit->input->password_re->setValue('<input type="password" name="password_re" class="form-control" value="" title="Re-Password" placeholder="Re-Password" required="required">');
		
			$form->edit->addInput('email','text');
			$form->edit->input->email->setTitle('Email');
			$form->edit->input->email->setType('email');
			$form->edit->input->email->setRequire();
			$form->edit->input->email->setUniq();
			
			$form->edit->addInput('phone','text');
			$form->edit->input->phone->setTitle('No HP');
			$form->edit->input->phone->setType('tel');
			$form->edit->input->phone->setRequire();
			$form->edit->input->phone->setUniq();
		}else{
			$form->edit->addInput('username', 'sqlplaintext');
			$form->edit->input->username->setTitle('Username');
			$form->edit->input->username->addTip($this->_tpl_model->button('user/data/usr', 'Ubah', 'fa fa-repeat', 'btn-sm modal_reload', '', 1));

			$form->edit->addInput('password', 'plaintext');
			$form->edit->input->password->setTitle('Password');
			$form->edit->input->password->setValue($this->_tpl_model->button('user/data/pwd', 'Ubah', 'fa fa-repeat', 'btn-sm modal_reload', '', 1));

			$form->edit->addInput('email', 'sqlplaintext');
			$form->edit->input->email->setTitle('Email');
			$form->edit->input->email->addTip($this->_tpl_model->button('user/data/usr?act=email', 'Ubah', 'fa fa-repeat', 'btn-sm modal_reload', '', 1));

			$form->edit->addInput('phone', 'sqlplaintext');
			$form->edit->input->phone->setTitle('No HP');
			$form->edit->input->phone->addTip($this->_tpl_model->button('user/data/usr?act=phone', 'Ubah', 'fa fa-repeat', 'btn-sm modal_reload', '', 1));

			$telegram_conf = $this->_tpl_model->config_direct('telegram');
			
			if (!empty($telegram_conf['active'])) {
				$form->edit->addInput('telegram_data', 'sqlplaintext');
				$form->edit->input->telegram_data->setTitle('Telegram');
				$form->edit->input->telegram_data->setDisplayFunction(function($value='')
				{
					$value = json_decode($value, 1);
					if ($value) {
						return '@'.$value['username'].' '.$value['first_name'].' '.$value['last_name'];
					}
				});
				$form->edit->input->telegram_data->addTip($this->_tpl_model->button('user/data/usr?act=telegram', 'Sambungkan', 'fa fa-link', 'btn-sm modal_reload', '', 1));
			}
		}

		$form->edit->addInput('image', 'file');
		$form->edit->input->image->setTitle('Foto');
		$form->edit->input->image->setFolder('files/user/');
		$form->edit->input->image->setResize(1080);
		$form->edit->input->image->setImageClick();
		$form->edit->input->image->setThumbnail(120, 'thumb/');
		$form->edit->input->image->setNameEncode();

		$form->edit->addInput('gender', 'select');
		$form->edit->input->gender->setTitle('Jenis Kelamin');
		$form->edit->input->gender->addOption('-- Pilih Jenis Kelamin --', '');
		$form->edit->input->gender->addOption('Laki-laki', 1);
		$form->edit->input->gender->addOption('Perempuan', 2);
		$form->edit->input->gender->addOption('Lainnya', 3);
		$form->edit->input->gender->setRequire();

		$form->edit->addInput('birth_place', 'selecttable');
		$form->edit->input->birth_place->setTitle('Tempat Lahir');
		$form->edit->input->birth_place->setReferenceTable('location');
		$form->edit->input->birth_place->setReferenceField( 'title', 'title' );
		$form->edit->input->birth_place->setReferenceCondition( '`type_id`=2' );
		$form->edit->input->birth_place->setReferenceOrderBy( 'title' );
		$form->edit->input->birth_place->setReferenceCache('files/cache_sys/location/birth_place_');
		$form->edit->input->birth_place->setAutoCompleteLocal();
		$form->edit->input->birth_place->setRequire();

		$form->edit->addInput('birth_date', 'date');
		$form->edit->input->birth_date->setTitle('Tanggal Lahir');
		$form->edit->input->birth_date->setRequire();
		$form->edit->input->birth_date->setMaxDate('-7 YEARS');

		$form->edit->addInput('location_input', 'multiinput');
		$form->edit->input->location_input->setTitle('Lokasi');

		$form->edit->input->location_input->addInput('province_id', 'selecttable');
		$form->edit->input->location_input->element->province_id->setTitle('Provinsi');
		$form->edit->input->location_input->element->province_id->setReferenceTable('location');
		$form->edit->input->location_input->element->province_id->setReferenceField( 'title', 'id' );
		$form->edit->input->location_input->element->province_id->setReferenceCondition( '`type_id`=1' );
		$form->edit->input->location_input->element->province_id->setReferenceOrderBy( 'title' );
		$form->edit->input->location_input->element->province_id->setReferenceCache('files/cache_sys/location/province_');
		$form->edit->input->location_input->element->province_id->addOption( '-- Pilih Provinsi --', '' );
		$form->edit->input->location_input->element->province_id->setRequire();

		$form->edit->input->location_input->addInput('city_id', 'selecttable');
		$form->edit->input->location_input->element->city_id->setTitle('Kabupaten');
		$form->edit->input->location_input->element->city_id->setReferenceTable('location');
		$form->edit->input->location_input->element->city_id->setReferenceField( 'title', 'id' );
		$form->edit->input->location_input->element->city_id->setReferenceCondition( '`type_id`=2' );
		$form->edit->input->location_input->element->city_id->setReferenceOrderBy( 'title' );
		$form->edit->input->location_input->element->city_id->setReferenceCache('files/cache_sys/location/city_');
		$form->edit->input->location_input->element->city_id->setDependent( $form->edit->input->location_input->element->province_id->getName(), 'par_id' );
		$form->edit->input->location_input->element->city_id->addOption( '-- Pilih Kabupaten --', '' );
		$form->edit->input->location_input->element->city_id->setRequire();

		$form->edit->input->location_input->addInput('district_id', 'selecttable');
		$form->edit->input->location_input->element->district_id->setTitle('Kecamatan');
		$form->edit->input->location_input->element->district_id->setReferenceTable('location');
		$form->edit->input->location_input->element->district_id->setReferenceField( 'title', 'id' );
		$form->edit->input->location_input->element->district_id->setReferenceCondition( '`type_id`=3' );
		$form->edit->input->location_input->element->district_id->setReferenceOrderBy( 'title' );
		$form->edit->input->location_input->element->district_id->setReferenceCache('files/cache_sys/location/district_');
		$form->edit->input->location_input->element->district_id->setDependent( $form->edit->input->location_input->element->city_id->getName(), 'par_id' );
		$form->edit->input->location_input->element->district_id->addOption( '-- Pilih Kecamatan --', '' );
		$form->edit->input->location_input->element->district_id->setRequire();

		$form->edit->input->location_input->addInput('village_id', 'selecttable');
		$form->edit->input->location_input->element->village_id->setTitle('Desa');
		$form->edit->input->location_input->element->village_id->setReferenceTable('location');
		$form->edit->input->location_input->element->village_id->setReferenceField( 'title', 'id' );
		$form->edit->input->location_input->element->village_id->setReferenceCondition( '`type_id`=4' );
		$form->edit->input->location_input->element->village_id->setReferenceOrderBy( 'title' );
		$form->edit->input->location_input->element->village_id->setReferenceCache('files/cache_sys/location/village_');
		$form->edit->input->location_input->element->village_id->setDependent( $form->edit->input->location_input->element->district_id->getName(), 'par_id' );
		$form->edit->input->location_input->element->village_id->addOption( '-- Pilih Desa --', '' );
		$form->edit->input->location_input->element->village_id->setRequire();
			
		$form->edit->input->location_input->addInput('zip_code','textdependent');
		$form->edit->input->location_input->element->zip_code->setTitle('Kode Pos');
		$form->edit->input->location_input->element->zip_code->setReferenceTable('location');
		$form->edit->input->location_input->element->zip_code->setReferenceField( 'zip_code' );
		$form->edit->input->location_input->element->zip_code->setReferenceCache('files/cache_sys/location/zip_code_');
		$form->edit->input->location_input->element->zip_code->setDependent($form->edit->input->location_input->element->village_id->getName(), 'id');
		$form->edit->input->location_input->element->zip_code->setType('number');
		$form->edit->input->location_input->element->zip_code->setRequire();

		$form->edit->addInput('address', 'text');
		$form->edit->input->address->setTitle('Alamat');
		$form->edit->input->address->setRequire();
		$form->edit->input->address->addTip('Jln. Jendral Sudirman No.123 RT.05 RW.06');

		if ($id) {
			$group_ids = @(array)json_decode($this->_db_model->getOne('SELECT `group_ids` FROM `user` WHERE `id`='.$id), 1);
		}else{
			$group_ids = [$group_id];
		}
		if ($group_ids) {
			$global_field = $this->_db_model->getOne('SELECT 1 FROM `user_group` WHERE `id` IN('.implode(',',$group_ids).') AND `global_field`=1 LIMIT 1');
			if ($global_field) {
				$group_ids[] = 0;
			}
			
			$form->edit->addInput('params', 'params');
			$form->edit->input->params->setTitle('');

			$fields = $this->_db_model->getAll('SELECT `name`,`form`,`title`,`required`,`params` FROM `user_field` WHERE `group_id` IN('.implode(',',$group_ids).') ORDER BY `group_id`,`orderby`');
			$form->edit->input->params->setParams($fields);
		}

		if ($this->_tpl_model->method == 'register') {
			$form->edit->addInput('captcha', 'captcha');
			$form->edit->input->captcha->setOpt('word_length', 5);
			$form->edit->input->captcha->setOpt('img_height', 50);
		}
		
		$form->edit->onSave(function($id, $f)
		{
			$data = $f->db->getRow('SELECT * FROM `user` WHERE `id`='.$id);
			if ($data) {
				$location    = $f->db->getAssoc('SELECT `id`,`title` FROM `location` WHERE `id` IN('.$data['province_id'].','.$data['city_id'].','.$data['district_id'].','.$data['village_id'].')');
				$data_update = array(
					'province_title' => @$location[$data['province_id']]['title'],
					'city_title'     => @$location[$data['city_id']]['title'],
					'district_title' => @$location[$data['district_id']]['title'],
					'village_title'  => @$location[$data['village_id']]['title'],
				);
				$data_address                = $data_update;
				$data_address['province_id'] = $data['province_id'];
				$data_address['city_id']     = $data['city_id'];
				$data_address['district_id'] = $data['district_id'];
				$data_address['village_id']  = $data['village_id'];
				$data_address['zip_code']    = $data['zip_code'];
				$data_address['address']     = $data['address'];
				$data_address['email']       = $data['email'];
				$data_address['phone']       = $data['phone'];
				$data_address['title']       = $data['name'];

				$address_id = $f->db->getOne('SELECT `id` FROM `user_address` WHERE `user_id`='.$id.' AND `main`=1');
				if ($address_id) {
					$f->db->update('user_address', $data_address, $address_id);
				}else{
					$data_address['user_id'] = $id;
					$data_address['main']    = 1;
					$f->db->insert('user_address', $data_address);
				}

				$group_id = $f->getData('group_id');
				if (empty($data['group_ids']) and $group_id) {
					$data_update['group_ids'] = json_encode(array((string)$group_id));
				}
				$f->db->update('user', $data_update, $id);
				if (isset($_POST['pwd'])) {
					if ($this->_tpl_model->user_login($data['username'], $_POST['pwd'])) {
						if (empty($_SESSION['user_return'])) {
							$url = $this->_tpl_model->_url.$this->_tpl_model->config('user','home_uri');
						}else{
							$url = $_SESSION['user_return'];
							unset($_SESSION['user_return']);
							if (isset($_SESSION['user_post'])) {
								$_SESSION['user_post_load'] = $_SESSION['user_post'];
								unset($_SESSION['user_post']);
							}
						}
						redirect($url);
					}
				}
			}
		});
		if (!$id) {
			$form->edit->setSaveButton('<i class="far fa-paper-plane"></i> Submit');
		}
		$form->edit->action();
		echo $form->edit->getForm();
		$this->_tpl_model->show();
	}

	function pwd()
	{
		$id = @intval($this->_tpl_model->user['id']);
		$this->_tpl_model->setLayout('blank');
		if ($id) {
			$form = $this->_pea_model->newForm('user');
			
			$form->initEdit('WHERE `id`='.$id);
			
			$form->edit->setHeader('Ubah Password');
			$form->edit->setModalResponsive();

			$form->edit->addInput('password','text');
			$form->edit->input->password->setTitle('Password Baru');
			$form->edit->input->password->setType('password');
			$form->edit->input->password->setRequire();
			$name = $form->edit->input->password->getName();
			if (!empty($_POST[$name])) {
				if ($_POST[$name] != @$_POST['password_re']) {
					$form->edit->input->password->msg = str_replace('{msg}', '<b>Password Baru dan Re-Password</b> tidak sama', $form->edit->input->password->failMsgTpl);
				}else{
					$this->load->model('_encrypt_model');
					$_POST[$name] = $this->_encrypt_model->encode($_POST[$name]);
				}
			}

			$form->edit->addInput('password_re', 'plaintext');
			$form->edit->input->password_re->setTitle('Re-Password');
			$form->edit->input->password_re->setValue('<input type="password" name="password_re" class="form-control" value="" title="Re-Password" placeholder="Re-Password" required="required">');

			$form->edit->addInput('password_current', 'plaintext');
			$form->edit->input->password_current->setTitle('Password Lama');
			$form->edit->input->password_current->setValue('<input type="password" name="password_current" class="form-control" value="" title="Password Lama" placeholder="Password Lama" required="required">');
			if (!empty($_POST[$name]) and !empty($_POST['password_current'])) {
				if (!$this->_tpl_model->user_pwd_validate($_POST['password_current'])) {
					$form->edit->input->password->msg = str_replace('{msg}', '<b>Password Lama tidak valid', $form->edit->input->password->failMsgTpl);
				}
			}

			$form->edit->onSave(function($id, $f)
			{
				$pwd = $f->db->getOne('SELECT `password` FROM `user` WHERE `id`='.$id);
				if ($pwd) {
					$_SESSION['user_login'][0]['password'] = $pwd;
				}
			});
			$form->edit->action();
			$form->edit->input->password->setValue('');
			echo $form->edit->getForm();
			$this->_tpl_model->show();
		}
	}

	function usr()
	{
		$id = @intval($this->_tpl_model->user['id']);
		$this->_tpl_model->setLayout('blank');
		if ($id) {
			$form = $this->_pea_model->newForm('user');
			
			$form->initEdit('WHERE `id`='.$id);
			
			$form->edit->setModalResponsive();

			switch (@$_GET['act']) {
				case 'email':
					$form->edit->setHeader('Ubah Email');
					
					$form->edit->addInput('email','text');
					$form->edit->input->email->setTitle('Email');
					$form->edit->input->email->setType('email');
					$form->edit->input->email->setRequire();
					$form->edit->input->email->setUniq();

					$input = 'email';
					break;

				case 'phone':
					$form->edit->setHeader('Ubah No HP');
					
					$form->edit->addInput('phone','text');
					$form->edit->input->phone->setTitle('No HP');
					$form->edit->input->phone->setType('tel');
					$form->edit->input->phone->setRequire();
					$form->edit->input->phone->setUniq();

					$input = 'phone';
					break;		

				case 'telegram':
					$form->edit->setHeader('Sambungkan Akun Telegram');
					
					$form->edit->addInput('telegram_id','hidden');
					$form->edit->input->telegram_id->addAttr('id="telegram_id"');
					
					$form->edit->addInput('telegram_data','hidden');
					$form->edit->input->telegram_data->addAttr('id="telegram_data"');

					$telegram_conf = $this->_tpl_model->config_direct('telegram');
					
					if ($id == $this->_tpl_model->user['id'] and !empty($telegram_conf['active'])) {
						$telegram_conf['data'] = @(array)json_decode($telegram_conf['data']);

						if (!empty($telegram_conf['data']['username'])) {
							$this->_tpl_model->js('controllers/admin/user/usr_telegram.js');
							$code = time().mt_rand();

							$form->edit->addInput('telegram_input','plaintext');
							$form->edit->input->telegram_input->setTitle('Akun Telegram');
							$form->edit->input->telegram_input->setValue('<input id="telegram_input" type="text" class="form-control" value="" title="Akun Telegram" placeholder="Akun Telegram" data-code="'.$code.'" data-url="https://api.telegram.org/bot'.$telegram_conf['token'].'/getUpdates" readonly>');

							$form->edit->addInput('telegram_instructions','plaintext');
							$form->edit->input->telegram_instructions->setTitle('Petunjuk');
							$form->edit->input->telegram_instructions->setValue('<ol><li>Klik <a href="https://t.me/'.$telegram_conf['data']['username'].'?start='.$code.'" target="_BLANK">Disini</a> untuk membuka Akun Telegram Kami.</li><li>Klik <b>Start</b> pada kotak pesan.</li><li>Tunggu sebentar dan lihat Akun Telegram Anda akan muncul diatas.</li><li>Lalu Simpan !</li></ol>');
						}
					}else{
						$form->edit->setSaveTool(false);
					}

					$input = 'telegram_data';
					break;		
				
				default:
					$form->edit->setHeader('Ubah Username');
					
					$form->edit->addInput('username','text');
					$form->edit->input->username->setTitle('Username');
					$form->edit->input->username->setRequire();
					$form->edit->input->username->setUniq();

					$input = 'username';
					break;
			}

			$form->edit->addInput('password_current', 'plaintext');
			$form->edit->input->password_current->setTitle('Password');
			$form->edit->input->password_current->setValue('<input type="password" name="password_current" class="form-control" value="" title="Password" placeholder="Password" required="required">');
			$name = $form->edit->input->$input->getName();
			if (!empty($_POST[$name]) and !empty($_POST['password_current'])) {
				if (!$this->_tpl_model->user_pwd_validate($_POST['password_current'])) {
					$form->edit->input->$input->msg = str_replace('{msg}', '<b>Password tidak valid', $form->edit->input->$input->failMsgTpl);
				}
			}

			$form->edit->action();
			echo $form->edit->getForm();
			$this->_tpl_model->show();
		}
	}
}
