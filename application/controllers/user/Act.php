<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Act extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		if (!in_array($this->_tpl_model->method, ['logout','detail']) and !empty($_SESSION['user_login'][0])) {
			$this->_tpl_model->user_login_validate();
		}
	}

	function login()
	{
		$this->load->model('_encrypt_model');
		$input = array(
			'usr' => mt_rand(100000000000,500000000000),
			'pwd' => mt_rand(500000000001,900000000000),
			'msg' => '',
		);
		$is_ok = 0;
		if (!empty($_POST['token'])) {
			$token = $this->_encrypt_model->decodeToken($_POST['token']);
			if ($token) {
				$token = explode('|', $token);
				if (count($token) == 2) {
					if (!empty($_POST[$token[0]]) and !empty($_POST[$token[1]])) {
						if ($this->_tpl_model->user_login($_POST[$token[0]], $_POST[$token[1]], 0, isset($_POST['remember']))) {
							$is_ok = 1;
						}else{
							$input['msg'] = $this->_tpl_model->msg($this->_tpl_model->user_msg(), 'danger');
						}
					}
				}
			}else{
				$input['msg'] = $this->_tpl_model->msg('Token Expired', 'danger');
			}
		}
		
		if (isset($_GET['acc'])) {
			if ($this->_tpl_model->user_login_with($_GET['acc'])) {
				$is_ok = 1;
			}else{
				$input['msg'] = $this->_tpl_model->msg($this->_tpl_model->user_msg(), 'danger');
			}
		}

		if ($this->_tpl_model->user_login_remember()) {
			$is_ok = 1;
		}

		if ($is_ok) {
			if (empty($_SESSION['user_return'])) {
				$url = $this->_tpl_model->_url.$this->_tpl_model->config('user','home_uri');
			}else{
				$url = $_SESSION['user_return'];
				unset($_SESSION['user_return']);
				if (isset($_SESSION['user_post'])) {
					$_SESSION['user_post_load'] = $_SESSION['user_post'];
					unset($_SESSION['user_post']);
				}
			}
			redirect($url);
		}
		
		$input['token'] = $this->_encrypt_model->encodeToken($input['usr'].'|'.$input['pwd'], 2);

		$this->_tpl_model->view('user/act/login', ['input' => $input]);
		$this->_tpl_model->show();
	}
	function logout()
	{
		$this->_tpl_model->user_logout();
		redirect($this->_tpl_model->_url.'user/act/login');
	}
	function forget()
	{
		$input = array(
			'msg' => '',
			'act' => 'search',
		);

		$input['dt'] = $this->_tpl_model->user_forget_pwd(@$_POST['search'],@$_POST['acc'],@$_POST['code']);
		if ($input['dt']) {
			if (isset($_SESSION['user_forget_pwd'])) {
				if (isset($_GET['reset'])) {
					unset($_SESSION['user_forget_pwd']);
					if (isset($_SESSION['user_forget_pwd_success'])) {
						unset($_SESSION['user_forget_pwd_success']);
					}
					redirect($this->_tpl_model->_url.'user/act/forget');
				}
				if (isset($_SESSION['user_forget_pwd_success'])) {
					$input['act'] = 'success';
				}else{
					$input['act'] = 'validate';
				}
			}else{
				$input['act'] = 'send';
			}
		}
		$input['msg'] = $this->_tpl_model->user_msg();
		if ($input['msg']) {
			$input['msg'] = $this->_tpl_model->msg($input['msg'], 'danger');
		}

		$this->_tpl_model->view('user/act/forget', ['input' => $input]);
		$this->_tpl_model->show();
	}

	function detail()
	{
		$id = @intval($_GET['id']);
		$this->_tpl_model->setLayout('blank');
		if ($id) {
			$form = $this->_pea_model->newForm('user');
			$form->initEdit('WHERE `id`='.$id);

			$form->edit->setHeader('User Detail');
			$form->edit->setModalResponsive();

			$form->edit->addInput('name', 'sqlplaintext');
			$form->edit->input->name->setTitle('Name');

			$form->edit->addInput('email', 'sqlplaintext');
			$form->edit->input->email->setTitle('Email');

			$form->edit->addInput('phone', 'sqlplaintext');
			$form->edit->input->phone->setTitle('Phone');

			$form->edit->addInput('image', 'file');
			$form->edit->input->image->setTitle('Image');
			$form->edit->input->image->setFolder('files/user/');
			$form->edit->input->image->setImageClick();
			$form->edit->input->image->setPlainText(true);

			$form->edit->addInput('gender', 'select');
			$form->edit->input->gender->setTitle('Gender');
			$form->edit->input->gender->addOption('Male', 1);
			$form->edit->input->gender->addOption('Female', 2);
			$form->edit->input->gender->setPlainText();

			$form->edit->addInput('birth_date', 'sqlplaintext');
			$form->edit->input->birth_date->setTitle('Place and Date of Birth');
			$form->edit->input->birth_date->setFieldName('CONCAT(`birth_place`,", ",DATE_FORMAT(`birth_date`, "%d %M %Y"))');

			$form->edit->addInput('location_detail', 'sqlplaintext');
			$form->edit->input->location_detail->setTitle('Address');
			$form->edit->input->location_detail->setFieldName('CONCAT(`address`," ",`village_title`,", ",`district_title`,", ",`city_title`,", ",`province_title`,", ",`zip_code`)');

			$form->edit->addInput('params', 'params');
			$form->edit->input->params->setTitle('');
			$form->edit->input->params->showParams();
			
			$form->edit->setSaveTool(false);
			$form->edit->action();
			echo $form->edit->getForm();
		}	
		$this->_tpl_model->show();
	}
}
