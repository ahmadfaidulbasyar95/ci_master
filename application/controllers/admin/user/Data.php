<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->menu_unprotect('profile');
		$this->_tpl_model->menu_unprotect('my_usr');
		$this->_tpl_model->menu_unprotect('my_pwd');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function index()
	{
		if (isset($_GET['login_auto'])) {
			$id = @intval($_GET['id']);
			if ($id) {
				$this->load->model('_encrypt_model');
				$id = $this->_encrypt_model->encodeToken($id, 5);
				redirect('admin/user/act/login_auto?id='.urlencode($id));
			}
		}

		$form = $this->_pea_model->newForm('user');

		$form->initSearch();

		$form->search->addInput('group_ids', 'selecttable');
		$form->search->input->group_ids->setTitle('Group');
		$form->search->input->group_ids->setReferenceTable('user_group');
		$form->search->input->group_ids->setReferenceField('title', 'id');
		$form->search->input->group_ids->addOption('-- Select Group --', '');
		$form->search->input->group_ids->setSearchFunction(function($value='')
		{
			return '`group_ids` LIKE "%\"'.$value.'\"%"';
		});
		if ($this->_tpl_model->user['id'] != 1) {
			$form->search->input->group_ids->setReferenceCondition('id != 1');
		}

		$form->search->addInput('keyword', 'keyword');
		$form->search->input->keyword->setTitle('Search');
		$form->search->input->keyword->addSearchField('name,username,email,phone,province_title,city_title,district_title,village_title,address,zip_code');
				
		$add_sql = $form->search->action();
		$keyword = $form->search->keyword();
		
		echo $form->search->getForm();

		echo $this->_tpl_model->button('admin/user/data/form_add', 'Add User', 'fa fa-plus', 'modal_reload', 'style="margin-right: 10px; margin-bottom: 15px;width: 100px;"', 1);
		
		if ($this->_tpl_model->user['id'] == 1) {
			echo $this->_tpl_model->button('admin/user/group?return='.urlencode($this->_tpl_model->_url_current), 'Group', 'fa fa-pencil', '', 'style="margin-right: 10px; margin-bottom: 15px;width: 100px;"');
		}

		$form->initRoll($add_sql.' ORDER BY `id` DESC');

		$form->roll->addInput('name', 'sqllinks');
		$form->roll->input->name->setTitle('Name');
		$form->roll->input->name->setLinks('admin/user/data/form');
		$form->roll->input->name->setModal();
		$form->roll->input->name->setModalReload();
		$form->roll->input->name->setDisplayColumn();

		$form->roll->addInput('group_ids', 'multiselect');
		$form->roll->input->group_ids->setTitle('Group');
		$form->roll->input->group_ids->setReferenceTable('user_group');
		$form->roll->input->group_ids->setReferenceField('title', 'id');
		$form->roll->input->group_ids->setPlainText();
		$form->roll->input->group_ids->setDisplayColumn();

		$form->roll->addInput('username', 'sqlplaintext');
		$form->roll->input->username->setTitle('Username');
		$form->roll->input->username->setDisplayColumn();

		$form->roll->addInput('email', 'sqlplaintext');
		$form->roll->input->email->setTitle('Email');
		$form->roll->input->email->setDisplayColumn();

		$form->roll->addInput('phone', 'sqlplaintext');
		$form->roll->input->phone->setTitle('Phone');
		$form->roll->input->phone->setDisplayColumn();

		$form->roll->addInput('image', 'file');
		$form->roll->input->image->setTitle('Image');
		$form->roll->input->image->setFolder('files/user/');
		$form->roll->input->image->setResize(1080);
		$form->roll->input->image->setImageClick();
		$form->roll->input->image->setThumbnail(120, 'thumb/');
		$form->roll->input->image->setPlainText(true);
		$form->roll->input->image->setDisplayColumn();

		$form->roll->addInput('gender', 'select');
		$form->roll->input->gender->setTitle('Gender');
		$form->roll->input->gender->addOption('Male', 1);
		$form->roll->input->gender->addOption('Female', 2);
		$form->roll->input->gender->addOption('Other', 3);
		$form->roll->input->gender->setPlainText();
		$form->roll->input->gender->setDisplayColumn(false);

		$form->roll->addInput('birth_place', 'sqlplaintext');
		$form->roll->input->birth_place->setTitle('Birthplace');
		$form->roll->input->birth_place->setDisplayColumn(false);

		$form->roll->addInput('birth_date', 'date');
		$form->roll->input->birth_date->setTitle('Birthdate');
		$form->roll->input->birth_date->setDateFormat('d M Y');
		$form->roll->input->birth_date->setPlainText();
		$form->roll->input->birth_date->setDisplayColumn(false);

		$form->roll->addInput('location_detail', 'sqlplaintext');
		$form->roll->input->location_detail->setTitle('Address');
		$form->roll->input->location_detail->setFieldName('CONCAT(`address`," ",`village_title`,", ",`district_title`,", ",`city_title`,", ",`province_title`,", ",`zip_code`)');
		$form->roll->input->location_detail->setDisplayColumn();

		$form->roll->addInput('login_wrap', 'multiinput');
		$form->roll->input->login_wrap->setTitle('Login');
		$form->roll->input->login_wrap->setDisplayColumn();
		$form->roll->input->login_wrap->setDelimiter('&nbsp;&nbsp;&nbsp;');

		$form->roll->input->login_wrap->addInput('login_act', 'editlinks');
		$form->roll->input->login_wrap->element->login_act->setTitle('<i class="fa fa-sign-in" title="Login with this account"></i>');
		$form->roll->input->login_wrap->element->login_act->setLinks('admin/user/data?login_auto=1');
		$form->roll->input->login_wrap->element->login_act->addAttr('target="_BLANK"');
		$form->roll->input->login_wrap->element->login_act->addAttr('onclick="return confirm(\'Login now ?\')"');

		$form->roll->input->login_wrap->addInput('login_log', 'editlinks');
		$form->roll->input->login_wrap->element->login_log->setTitle('<i class="fa fa-clock" title="Login History"></i>');
		$form->roll->input->login_wrap->element->login_log->setLinks('admin/user/log');

		$form->roll->addInput('active', 'checkbox');
		$form->roll->input->active->setTitle('Active');
		$form->roll->input->active->setCaption('yes');
		$form->roll->input->active->setDisplayColumn();

		$form->roll->addInput('created', 'datetime');
		$form->roll->input->created->setTitle('Created');
		$form->roll->input->created->setPlainText();
		$form->roll->input->created->setDisplayColumn();

		$form->roll->addInput('updated', 'datetime');
		$form->roll->input->updated->setTitle('Updated');
		$form->roll->input->updated->setPlainText();
		$form->roll->input->updated->setDisplayColumn(false);
		
		$form->roll->setDeleteCondition('{roll_id} == '.$this->_tpl_model->user['id']);
		$form->roll->setDeleteCondition('{roll_id} == 1');

		$form->roll->onDelete(function($id, $f)
		{
			lib_path_delete($f->_root.'files/params/user/'.$id);
		});
		$form->roll->onSave(function($id, $f)
		{
			if ($id == 1) {
				if (!$f->db->getOne('SELECT `active` FROM `user` WHERE `id`=1')) {
					$f->db->update('user', array('active' => 1), 1);
				}
			}
		});
		$form->roll->addReportAll();
		$form->roll->action();
		echo $form->roll->getForm();
		$this->_tpl_model->show();
	}

	function profile()
	{
		$_GET['id']    = $this->_tpl_model->user['id'];
		$_GET['title'] = 'Profile';
		$this->form();
	}

	function form_add()
	{
		$_GET['return'] = '';
		$this->_tpl_model->setLayout('blank');

		$form = $this->_pea_model->newForm('user');
		$form->initEdit();

		$form->edit->setHeader('Select Group');
		$form->edit->setModalResponsive();
		
		$form->edit->addInput('group_id','selecttable');
		$form->edit->input->group_id->setTitle('');
		$form->edit->input->group_id->setReferenceTable('user_group');
		$form->edit->input->group_id->setReferenceField('title', 'id');
		$form->edit->input->group_id->setRequire();
		if ($this->_tpl_model->user['id'] != 1) {
			$form->edit->input->group_id->setReferenceCondition('id != 1');
		}
		$group_id = $form->edit->input->group_id->getName();
		if (isset($_POST[$group_id])) {
			redirect('admin/user/data/form?group_id='.$_POST[$group_id]);
		}
				
		$form->edit->setSaveButton('<i class="fa fa-send"></i> Next');
		$form->edit->action();
		echo $form->edit->getForm();
		$this->_tpl_model->show();
	}

	function form()
	{
		$id   = @intval($_GET['id']);
		$form = $this->_pea_model->newForm('user');

		$_GET['return'] = '';
		$this->_tpl_model->setLayout('blank');

		$form->initEdit(!empty($id) ? 'WHERE `id`='.$id : '');
		
		if (isset($_GET['title'])) {
			$form->edit->setHeader($_GET['title']);
		}else{
			$form->edit->setHeader(!empty($id) ? 'Edit User' : 'Add User');
		}
		$form->edit->setModalResponsive();
		
		$form->edit->addInput('name','text');
		$form->edit->input->name->setTitle('Name');
		$form->edit->input->name->setRequire();

		if (!$id) {
			$group_id    = @intval($_GET['group_id']);
			$group_title = $this->_db_model->getOne('SELECT `title` FROM `user_group` WHERE `id`='.$group_id);
			if (!$group_title) {
				redirect('admin/user/data/form_add');
			}
			$_GET['return'] = $this->_tpl_model->_url.'admin/user/data/form_add';

			$form->edit->addExtraField('group_ids', '["'.$group_id.'"]');

			$form->edit->addInput('group_title', 'plaintext');
			$form->edit->input->group_title->setTitle('Group');
			$form->edit->input->group_title->setValue($group_title);
					
			$form->edit->addInput('username','text');
			$form->edit->input->username->setTitle('Username');
			$form->edit->input->username->setRequire();
			$form->edit->input->username->setUniq();
		
			$form->edit->addInput('password','text');
			$form->edit->input->password->setTitle('Password');
			$form->edit->input->password->setType('password');
			$form->edit->input->password->setRequire();
			$name = $form->edit->input->password->getName();
			if (!empty($_POST[$name])) {
				if ($_POST[$name] != @$_POST['password_re']) {
					$form->edit->input->password->msg = str_replace('{msg}', '<b>Password and Re-Password</b> does not Match', $form->edit->input->password->failMsgTpl);
				}else{
					$this->load->model('_encrypt_model');
					$_POST[$name] = $this->_encrypt_model->encode($_POST[$name]);
				}
			}

			$form->edit->addInput('password_re', 'plaintext');
			$form->edit->input->password_re->setTitle('Re-Password');
			$form->edit->input->password_re->setValue('<input type="password" name="password_re" class="form-control" value="" title="Re-Password" placeholder="Re-Password" required="required">');
		
			$form->edit->addInput('email','text');
			$form->edit->input->email->setTitle('Email');
			$form->edit->input->email->setType('email');
			$form->edit->input->email->setRequire();
			$form->edit->input->email->setUniq();
			
			$form->edit->addInput('phone','text');
			$form->edit->input->phone->setTitle('Phone');
			$form->edit->input->phone->setType('tel');
			$form->edit->input->phone->setRequire();
			$form->edit->input->phone->setUniq();
		}else{
			$form->edit->addInput('group_ids', 'multiselect');
			$form->edit->input->group_ids->setTitle('Group');
			$form->edit->input->group_ids->setReferenceTable('user_group');
			$form->edit->input->group_ids->setReferenceField('title', 'id');
			$form->edit->input->group_ids->setPlainText();

			$form->edit->addInput('username', 'sqlplaintext');
			$form->edit->input->username->setTitle('Username');

			$form->edit->addInput('password', 'plaintext');
			$form->edit->input->password->setTitle('Password');

			$form->edit->addInput('email', 'sqlplaintext');
			$form->edit->input->email->setTitle('Email');

			$form->edit->addInput('phone', 'sqlplaintext');
			$form->edit->input->phone->setTitle('Phone');

			if ($this->_tpl_model->method == 'profile') {
				$form->edit->input->group_ids->addTip($this->_tpl_model->button('admin/user/data/my_usr?act=group_ids', 'Change', 'fa fa-repeat', 'btn-sm modal_reload', '', 1));
				$form->edit->input->username->addTip($this->_tpl_model->button('admin/user/data/my_usr', 'Change', 'fa fa-repeat', 'btn-sm modal_reload', '', 1));
				$form->edit->input->password->setValue($this->_tpl_model->button('admin/user/data/my_pwd', 'Change', 'fa fa-repeat', 'btn-sm modal_reload', '', 1));
				$form->edit->input->email->addTip($this->_tpl_model->button('admin/user/data/my_usr?act=email', 'Change', 'fa fa-repeat', 'btn-sm modal_reload', '', 1));
				$form->edit->input->phone->addTip($this->_tpl_model->button('admin/user/data/my_usr?act=phone', 'Change', 'fa fa-repeat', 'btn-sm modal_reload', '', 1));
			}else{
				$form->edit->input->group_ids->addTip($this->_tpl_model->button('admin/user/data/usr?id='.$id.'&act=group_ids&return='.urlencode($this->_tpl_model->_url_current), 'Change', 'fa fa-repeat', 'btn-sm'));
				$form->edit->input->username->addTip($this->_tpl_model->button('admin/user/data/usr?id='.$id.'&return='.urlencode($this->_tpl_model->_url_current), 'Change', 'fa fa-repeat', 'btn-sm'));
				$form->edit->input->password->setValue($this->_tpl_model->button('admin/user/data/pwd?id='.$id.'&return='.urlencode($this->_tpl_model->_url_current), 'Change', 'fa fa-repeat', 'btn-sm'));
				$form->edit->input->email->addTip($this->_tpl_model->button('admin/user/data/usr?id='.$id.'&act=email&return='.urlencode($this->_tpl_model->_url_current), 'Change', 'fa fa-repeat', 'btn-sm'));
				$form->edit->input->phone->addTip($this->_tpl_model->button('admin/user/data/usr?id='.$id.'&act=phone&return='.urlencode($this->_tpl_model->_url_current), 'Change', 'fa fa-repeat', 'btn-sm'));
			}

			$telegram_conf = $this->_tpl_model->config_direct('telegram');
			
			if (!empty($telegram_conf['active'])) {
				$form->edit->addInput('telegram_data', 'sqlplaintext');
				$form->edit->input->telegram_data->setTitle('Telegram');
				$form->edit->input->telegram_data->setDisplayFunction(function($value='')
				{
					$value = json_decode($value, 1);
					if ($value) {
						return '@'.$value['username'].' '.$value['first_name'].' '.$value['last_name'];
					}
				});
				if ($this->_tpl_model->method == 'profile') {
					$form->edit->input->telegram_data->addTip($this->_tpl_model->button('admin/user/data/my_usr?act=telegram', 'Connect', 'fa fa-link', 'btn-sm modal_reload', '', 1));
				}else{
					$form->edit->input->telegram_data->addTip($this->_tpl_model->button('admin/user/data/usr?id='.$id.'&act=telegram&return='.urlencode($this->_tpl_model->_url_current), 'Connect', 'fa fa-link', 'btn-sm'));
				}
			}
		}

		$form->edit->addInput('image', 'file');
		$form->edit->input->image->setTitle('Image');
		$form->edit->input->image->setFolder('files/user/');
		$form->edit->input->image->setResize(1080);
		$form->edit->input->image->setImageClick();
		$form->edit->input->image->setThumbnail(120, 'thumb/');
		$form->edit->input->image->setNameEncode();

		$form->edit->addInput('gender', 'select');
		$form->edit->input->gender->setTitle('Gender');
		$form->edit->input->gender->addOption('-- Select Gender --', '');
		$form->edit->input->gender->addOption('Male', 1);
		$form->edit->input->gender->addOption('Female', 2);
		$form->edit->input->gender->addOption('Other', 3);
		$form->edit->input->gender->setRequire();

		$form->edit->addInput('birth_place', 'selecttable');
		$form->edit->input->birth_place->setTitle('Birthplace');
		$form->edit->input->birth_place->setReferenceTable('location');
		$form->edit->input->birth_place->setReferenceField( 'title', 'title' );
		$form->edit->input->birth_place->setReferenceCondition( '`type_id`=2' );
		$form->edit->input->birth_place->setReferenceOrderBy( 'title' );
		$form->edit->input->birth_place->setReferenceCache('files/cache_sys/location/birth_place_');
		$form->edit->input->birth_place->setAutoCompleteLocal();
		$form->edit->input->birth_place->setRequire();

		$form->edit->addInput('birth_date', 'date');
		$form->edit->input->birth_date->setTitle('Birthdate');
		$form->edit->input->birth_date->setRequire();
		$form->edit->input->birth_date->setMaxDate('-7 YEARS');

		$form->edit->addInput('location_input', 'multiinput');
		$form->edit->input->location_input->setTitle('Location');

		$form->edit->input->location_input->addInput('province_id', 'selecttable');
		$form->edit->input->location_input->element->province_id->setTitle('Province');
		$form->edit->input->location_input->element->province_id->setReferenceTable('location');
		$form->edit->input->location_input->element->province_id->setReferenceField( 'title', 'id' );
		$form->edit->input->location_input->element->province_id->setReferenceCondition( '`type_id`=1' );
		$form->edit->input->location_input->element->province_id->setReferenceOrderBy( 'title' );
		$form->edit->input->location_input->element->province_id->setReferenceCache('files/cache_sys/location/province_');
		$form->edit->input->location_input->element->province_id->addOption( '-- Select Province --', '' );
		$form->edit->input->location_input->element->province_id->setRequire();

		$form->edit->input->location_input->addInput('city_id', 'selecttable');
		$form->edit->input->location_input->element->city_id->setTitle('City');
		$form->edit->input->location_input->element->city_id->setReferenceTable('location');
		$form->edit->input->location_input->element->city_id->setReferenceField( 'title', 'id' );
		$form->edit->input->location_input->element->city_id->setReferenceCondition( '`type_id`=2' );
		$form->edit->input->location_input->element->city_id->setReferenceOrderBy( 'title' );
		$form->edit->input->location_input->element->city_id->setReferenceCache('files/cache_sys/location/city_');
		$form->edit->input->location_input->element->city_id->setDependent( $form->edit->input->location_input->element->province_id->getName(), 'par_id' );
		$form->edit->input->location_input->element->city_id->addOption( '-- Select City --', '' );
		$form->edit->input->location_input->element->city_id->setRequire();

		$form->edit->input->location_input->addInput('district_id', 'selecttable');
		$form->edit->input->location_input->element->district_id->setTitle('District');
		$form->edit->input->location_input->element->district_id->setReferenceTable('location');
		$form->edit->input->location_input->element->district_id->setReferenceField( 'title', 'id' );
		$form->edit->input->location_input->element->district_id->setReferenceCondition( '`type_id`=3' );
		$form->edit->input->location_input->element->district_id->setReferenceOrderBy( 'title' );
		$form->edit->input->location_input->element->district_id->setReferenceCache('files/cache_sys/location/district_');
		$form->edit->input->location_input->element->district_id->setDependent( $form->edit->input->location_input->element->city_id->getName(), 'par_id' );
		$form->edit->input->location_input->element->district_id->addOption( '-- Select District --', '' );
		$form->edit->input->location_input->element->district_id->setRequire();

		$form->edit->input->location_input->addInput('village_id', 'selecttable');
		$form->edit->input->location_input->element->village_id->setTitle('Village');
		$form->edit->input->location_input->element->village_id->setReferenceTable('location');
		$form->edit->input->location_input->element->village_id->setReferenceField( 'title', 'id' );
		$form->edit->input->location_input->element->village_id->setReferenceCondition( '`type_id`=4' );
		$form->edit->input->location_input->element->village_id->setReferenceOrderBy( 'title' );
		$form->edit->input->location_input->element->village_id->setReferenceCache('files/cache_sys/location/village_');
		$form->edit->input->location_input->element->village_id->setDependent( $form->edit->input->location_input->element->district_id->getName(), 'par_id' );
		$form->edit->input->location_input->element->village_id->addOption( '-- Select Village --', '' );
		$form->edit->input->location_input->element->village_id->setRequire();
			
		$form->edit->input->location_input->addInput('zip_code','textdependent');
		$form->edit->input->location_input->element->zip_code->setTitle('Zip Code');
		$form->edit->input->location_input->element->zip_code->setReferenceTable('location');
		$form->edit->input->location_input->element->zip_code->setReferenceField( 'zip_code' );
		$form->edit->input->location_input->element->zip_code->setReferenceCache('files/cache_sys/location/zip_code_');
		$form->edit->input->location_input->element->zip_code->setDependent($form->edit->input->location_input->element->village_id->getName(), 'id');
		$form->edit->input->location_input->element->zip_code->setType('number');
		$form->edit->input->location_input->element->zip_code->setRequire();

		$form->edit->addInput('address', 'text');
		$form->edit->input->address->setTitle('Address');
		$form->edit->input->address->setRequire();
		$form->edit->input->address->addTip('Jln. Jendral Sudirman No.123 RT.05 RW.06');

		if ($id) {
			$group_ids = @(array)json_decode($this->_db_model->getOne('SELECT `group_ids` FROM `user` WHERE `id`='.$id), 1);
		}else{
			$group_ids = [$group_id];
		}
		if ($group_ids) {
			$global_field = $this->_db_model->getOne('SELECT 1 FROM `user_group` WHERE `id` IN('.implode(',',$group_ids).') AND `global_field`=1 LIMIT 1');
			if ($global_field) {
				$group_ids[] = 0;
			}
			
			$form->edit->addInput('params', 'params');
			$form->edit->input->params->setTitle('');

			$fields = $this->_db_model->getAll('SELECT `name`,`form`,`title`,`required`,`params` FROM `user_field` WHERE `group_id` IN('.implode(',',$group_ids).') ORDER BY `group_id`,`orderby`');
			$form->edit->input->params->setParams($fields);
		}

		$form->edit->addInput('active', 'checkbox');
		$form->edit->input->active->setTitle('Active');
		$form->edit->input->active->setCaption('yes');
		if ($id == 1) {
			$form->edit->input->active->setPlainText();
			if ($this->_tpl_model->user['id'] != 1) {
				$form->edit->input->name->setPlainText();
				$form->edit->input->image->setPlainText();
				$form->edit->input->gender->setPlainText();
				$form->edit->input->birth_place->setPlainText();
				$form->edit->input->birth_date->setPlainText();
				$form->edit->input->location_input->element->province_id->setPlainText();
				$form->edit->input->location_input->element->city_id->setPlainText();
				$form->edit->input->location_input->element->district_id->setPlainText();
				$form->edit->input->location_input->element->village_id->setPlainText();
				$form->edit->input->location_input->element->zip_code->setPlainText();
				$form->edit->input->address->setPlainText();
				$form->edit->input->address->setTip('');

				$form->edit->input->group_ids->setTip('');
				$form->edit->input->username->setTip('');
				$form->edit->input->email->setTip('');
				$form->edit->input->phone->setTip('');
				$form->edit->input->telegram_data->setTip('');

				$form->edit->input->password->setInputPosition('hidden');
				
				$form->edit->setSaveTool(false);
			}
		}

		if ($this->_tpl_model->method == 'profile') {
			$form->edit->input->active->setPlainText();
			$form->edit->input->group_ids->setTip('');
			$form->edit->input->address->addTip('<br>'.$this->_tpl_model->button('admin/user/address', 'Manage Address List', 'far fa-address-book', 'modal_reload modal_large', '', 1));
			$form->edit->input->active->addTip($this->_tpl_model->button('admin/user/log/me', 'Manage Active Sessions', 'far fa-user-shield', 'modal_large', '', 1));
		}
		
		$form->edit->onSave(function($id, $f)
		{
			$data = $f->db->getRow('SELECT * FROM `user` WHERE `id`='.$id);
			if ($data) {
				$location    = $f->db->getAssoc('SELECT `id`,`title` FROM `location` WHERE `id` IN('.$data['province_id'].','.$data['city_id'].','.$data['district_id'].','.$data['village_id'].')');
				$data_update = array(
					'province_title' => @$location[$data['province_id']]['title'],
					'city_title'     => @$location[$data['city_id']]['title'],
					'district_title' => @$location[$data['district_id']]['title'],
					'village_title'  => @$location[$data['village_id']]['title'],
				);
				$data_address                = $data_update;
				$data_address['province_id'] = $data['province_id'];
				$data_address['city_id']     = $data['city_id'];
				$data_address['district_id'] = $data['district_id'];
				$data_address['village_id']  = $data['village_id'];
				$data_address['zip_code']    = $data['zip_code'];
				$data_address['address']     = $data['address'];
				$data_address['email']       = $data['email'];
				$data_address['phone']       = $data['phone'];
				$data_address['title']       = $data['name'];

				$address_id = $f->db->getOne('SELECT `id` FROM `user_address` WHERE `user_id`='.$id.' AND `main`=1');
				if ($address_id) {
					$f->db->update('user_address', $data_address, $address_id);
				}else{
					$data_address['user_id'] = $id;
					$data_address['main']    = 1;
					$f->db->insert('user_address', $data_address);
				}

				$f->db->update('user', $data_update, $id);
			}
		});
		$form->edit->action();
		echo $form->edit->getForm();
		$this->_tpl_model->show();
	}

	function my_pwd()
	{
		$_GET['id'] = $this->_tpl_model->user['id'];
		$this->pwd();
	}
	function pwd()
	{
		$id = @intval($_GET['id']);
		$this->_tpl_model->setLayout('blank');
		if ($id) {
			$form = $this->_pea_model->newForm('user');
			
			$form->initEdit('WHERE `id`='.$id);
			
			$form->edit->setHeader('Change Password');
			$form->edit->setModalResponsive();

			$form->edit->addInput('name', 'sqlplaintext');
			$form->edit->input->name->setTitle('Name');

			$form->edit->addInput('username', 'sqlplaintext');
			$form->edit->input->username->setTitle('Username');

			if ($id == 1 and $this->_tpl_model->user['id'] != 1) {
				echo $this->_tpl_model->msg('Not Allowed', 'danger');
				$form->edit->setSaveTool(false);
			}else{
				$form->edit->addInput('password','text');
				$form->edit->input->password->setTitle('New Password');
				$form->edit->input->password->setType('password');
				$form->edit->input->password->setRequire();
				$name = $form->edit->input->password->getName();
				if (!empty($_POST[$name])) {
					if ($_POST[$name] != @$_POST['password_re']) {
						$form->edit->input->password->msg = str_replace('{msg}', '<b>Password and Re-Password</b> does not Match', $form->edit->input->password->failMsgTpl);
					}else{
						$this->load->model('_encrypt_model');
						$_POST[$name] = $this->_encrypt_model->encode($_POST[$name]);
					}
				}

				$form->edit->addInput('password_re', 'plaintext');
				$form->edit->input->password_re->setTitle('Re-Password');
				$form->edit->input->password_re->setValue('<input type="password" name="password_re" class="form-control" value="" title="Re-Password" placeholder="Re-Password" required="required">');

				$form->edit->action();
				$form->edit->input->password->setValue('');
			}			
			echo $form->edit->getForm();
			$this->_tpl_model->show();
		}
	}

	function my_usr()
	{
		$_GET['id'] = $this->_tpl_model->user['id'];
		$this->usr();
	}
	function usr()
	{
		$id = @intval($_GET['id']);
		$this->_tpl_model->setLayout('blank');
		if ($id) {
			$form = $this->_pea_model->newForm('user');
			
			$form->initEdit('WHERE `id`='.$id);
			
			$form->edit->setModalResponsive();

			$form->edit->addInput('name', 'sqlplaintext');
			$form->edit->input->name->setTitle('Name');

			switch (@$_GET['act']) {
				case 'group_ids':
					$form->edit->setHeader('Change Group');
					
					$form->edit->addInput('group_ids','multiselect');
					$form->edit->input->group_ids->setTitle('Group');
					$form->edit->input->group_ids->setReferenceTable('user_group');
					$form->edit->input->group_ids->setReferenceField('title', 'id');
					$form->edit->input->group_ids->setRequire();

					if ($id == 1 or $this->_tpl_model->user['id'] == $id) {
						$form->edit->input->group_ids->setPlainText();
						$form->edit->setSaveTool(false);
					}
					break;
				case 'email':
					$form->edit->setHeader('Change Email');
					
					$form->edit->addInput('email','text');
					$form->edit->input->email->setTitle('Email');
					$form->edit->input->email->setType('email');
					$form->edit->input->email->setRequire();
					$form->edit->input->email->setUniq();

					if ($id == 1 and $this->_tpl_model->user['id'] != 1) {
						$form->edit->input->email->setPlainText();
						$form->edit->setSaveTool(false);
					}
					break;

				case 'phone':
					$form->edit->setHeader('Change Phone');
					
					$form->edit->addInput('phone','text');
					$form->edit->input->phone->setTitle('Phone');
					$form->edit->input->phone->setType('tel');
					$form->edit->input->phone->setRequire();
					$form->edit->input->phone->setUniq();

					if ($id == 1 and $this->_tpl_model->user['id'] != 1) {
						$form->edit->input->phone->setPlainText();
						$form->edit->setSaveTool(false);
					}
					break;		

				case 'telegram':
					$form->edit->setHeader('Connect Telegram Account');
					
					$form->edit->addInput('telegram_id','hidden');
					$form->edit->input->telegram_id->addAttr('id="telegram_id"');
					
					$form->edit->addInput('telegram_data','hidden');
					$form->edit->input->telegram_data->addAttr('id="telegram_data"');

					$telegram_conf = $this->_tpl_model->config_direct('telegram');
					
					if ($id == $this->_tpl_model->user['id'] and !empty($telegram_conf['active'])) {
						$telegram_conf['data'] = @(array)json_decode($telegram_conf['data']);

						if (!empty($telegram_conf['data']['username'])) {
							$this->_tpl_model->js('controllers/admin/user/usr_telegram.js');
							$code = time().mt_rand();

							$form->edit->addInput('telegram_input','plaintext');
							$form->edit->input->telegram_input->setTitle('Telegram Account');
							$form->edit->input->telegram_input->setValue('<input id="telegram_input" type="text" class="form-control" value="" title="Telegram Account" placeholder="Telegram Account" data-code="'.$code.'" data-url="https://api.telegram.org/bot'.$telegram_conf['token'].'/getUpdates" readonly>');

							$form->edit->addInput('telegram_instructions','plaintext');
							$form->edit->input->telegram_instructions->setTitle('Instructions');
							$form->edit->input->telegram_instructions->setValue('<ol><li>Click <a href="https://t.me/'.$telegram_conf['data']['username'].'?start='.$code.'" target="_BLANK">Here</a> to open Our Telegram Account.</li><li>Click <b>Start</b> on the message box.</li><li>Wait a second and see Your Telegram Account appear on the top.</li><li>Then Save it !</li></ol>');
						}
					}else{
						$form->edit->setSaveTool(false);
					}
					break;		
				
				default:
					$form->edit->setHeader('Change Username');
					
					$form->edit->addInput('username','text');
					$form->edit->input->username->setTitle('Username');
					$form->edit->input->username->setRequire();
					$form->edit->input->username->setUniq();

					if ($id == 1 and $this->_tpl_model->user['id'] != 1) {
						$form->edit->input->username->setPlainText();
						$form->edit->setSaveTool(false);
					}
					break;
			}

			$form->edit->action();
			echo $form->edit->getForm();
			$this->_tpl_model->show();
		}
	}
}
