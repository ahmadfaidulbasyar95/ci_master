<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->menu_unprotect('me');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function me()
	{
		$_GET['id'] = $this->_tpl_model->user['id'];
		$this->index();
	}
	function index()
	{
		$id   = @intval($_GET['id']);
		$form = $this->_pea_model->newForm('user_log');

		if ($id) {
			$user = $this->_tpl_model->_db_model->getRow('SELECT `name`,`username` FROM `user` WHERE `id`='.$id);
			if ($user) {
				$this->_tpl_model->nav_add('admin/user/log?id='.$id, 'Login History');
				$this->_tpl_model->nav_add($user['name'].' ['.$user['username'].']');
				if ($id == $this->_tpl_model->user['id']) {
					$logout_set = @intval($_POST['logout_set']);
					if ($logout_set) {
						$this->_tpl_model->_db_model->update('user_log', array(
							'logout' => date('Y-m-d H:i:s'),
						), '`id`='.$logout_set.' AND `user_id`='.$id);
						echo $this->_tpl_model->msg('Session Ended', 'success');
					}
				}
			}
		}

		$form->initSearch();

		$form->search->addInput('type', 'select');
		$form->search->input->type->setTitle('Type');
		$form->search->input->type->addOption('-- Select Type --', '');
		$form->search->input->type->addOptions($this->_tpl_model->user_group_type);
		
		$form->search->addInput('keyword','keyword');
		$form->search->input->keyword->setTitle('Device');
		$form->search->input->keyword->addSearchField('device');
		
		$form->search->addInput('keyword2','keyword');
		$form->search->input->keyword2->setTitle('IP Address');
		$form->search->input->keyword2->addSearchField('ip');

		$form->search->addInput('login', 'dateinterval');
		$form->search->input->login->setTitle('Login Date');

		$form->search->addInput('logout', 'dateinterval');
		$form->search->input->logout->setTitle('Logout Date');
		
		$form->search->addExtraField('user_id', $id);

		$add_sql = $form->search->action();
		$keyword = $form->search->keyword();

		$form->initRoll($add_sql.' ORDER BY `id` DESC');

		if ($this->_tpl_model->method == 'me') {
			$this->_tpl_model->setLayout('blank');
			$form->roll->setHeader('Manage Active Sessions');
			$form->roll->bodyWrap($form->roll->formBodyBefore.$form->search->getForm(), $form->roll->formBodyAfter);
		}else{
			echo $form->search->getForm();
		}

		$form->roll->addInput('type', 'select');
		$form->roll->input->type->setTitle('Type');
		$form->roll->input->type->addOptions($this->_tpl_model->user_group_type);
		$form->roll->input->type->setPlainText();
		$form->roll->input->type->setDisplayColumn();
		
		$form->roll->addInput('device','sqlplaintext');
		$form->roll->input->device->setTitle('Device');
		$form->roll->input->device->setDisplayColumn();

		$form->roll->addInput('ip','sqlplaintext');
		$form->roll->input->ip->setTitle('IP Address');
		$form->roll->input->ip->setDisplayColumn();

		$form->roll->addInput('login', 'datetime');
		$form->roll->input->login->setTitle('Login Date');
		$form->roll->input->login->setPlainText();
		$form->roll->input->login->setDisplayColumn();

		$form->roll->addInput('logout', 'datetime');
		$form->roll->input->logout->setTitle('Logout Date');
		$form->roll->input->logout->setPlainText();
		$form->roll->input->logout->setDisplayColumn();
		if ($id == $this->_tpl_model->user['id']) {
			$form->roll->input->logout->setDisplayFunction(function($value='', $id=0, $index='', $values=array())
			{
				if ($values[$index]['logout'] == '0000-00-00 00:00:00') {
					if ($this->_tpl_model->user['log_id'] == $values[$index]['roll_id']) {
						$value = 'This Device';
					}else{
						$value = '<button class="btn btn-danger btn-xs" type="submit" name="logout_set" value="'.$values[$index]['roll_id'].'" onclick="return confirm(\'End Session ?\')">End Session</button>';
					}
				}
				return $value;
			});
		}
		
		$form->roll->setSaveTool(false);
		$form->roll->setDeleteTool(false);
		$form->roll->addReportAll();
		$form->roll->action();
		echo $form->roll->getForm();
		$this->_tpl_model->show();
	}
}
