<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Act extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function login()
	{
		if ($this->_tpl_model->_url_current != $this->_tpl_model->_url.$this->_tpl_model->config('dashboard', 'login_uri') and !isset($_GET['acc'])) {
			show_404();
		}
		$this->load->model('_encrypt_model');
		$input = array(
			'usr' => mt_rand(100000000000,500000000000),
			'pwd' => mt_rand(500000000001,900000000000),
			'msg' => '',
		);
		if (!empty($_POST['token'])) {
			$token = $this->_encrypt_model->decodeToken($_POST['token']);
			if ($token) {
				$token = explode('|', $token);
				if (count($token) == 2) {
					if (!empty($_POST[$token[0]]) and !empty($_POST[$token[1]])) {
						if ($this->_tpl_model->user_login($_POST[$token[0]], $_POST[$token[1]], 1, isset($_POST['remember']))) {
							redirect($this->_tpl_model->_url.'admin/dashboard');
						}else{
							$input['msg'] = $this->_tpl_model->msg($this->_tpl_model->user_msg(), 'danger');
						}
					}
				}
			}else{
				$input['msg'] = $this->_tpl_model->msg('Token Expired', 'danger');
			}
		}
		
		if (isset($_GET['acc'])) {
			if ($this->_tpl_model->user_login_with($_GET['acc'], 1)) {
				redirect($this->_tpl_model->_url.'admin/dashboard');
			}else{
				$input['msg'] = $this->_tpl_model->msg($this->_tpl_model->user_msg(), 'danger');
			}
		}

		if ($this->_tpl_model->user_login_remember(1)) {
			redirect($this->_tpl_model->_url.'admin/dashboard');
		}

		$input['token'] = $this->_encrypt_model->encodeToken($input['usr'].'|'.$input['pwd'], 2);

		$this->_tpl_model->setLayout('blank');
		$this->_tpl_model->view('user/act/login', ['input' => $input]);
		$this->_tpl_model->show();
	}
	function login_auto()
	{
		if (!empty($_GET['id'])) {
			$this->load->model('_encrypt_model');
			$id = $_GET['id'];
			$id = intval($this->_encrypt_model->decodeToken($id));
			if ($id) {
				$user = $this->_tpl_model->_db_model->getRow('SELECT `username`,`password` FROM `user` WHERE `id`='.$id);
				if ($user) {
					$this->load->model('_encrypt_model');
					$user['password'] = $this->_encrypt_model->decode($user['password']);
					if ($this->_tpl_model->user_login($user['username'], $user['password'])) {
						redirect($this->_tpl_model->_url.$this->_tpl_model->config('user','home_uri'));
					}else{
						echo $this->_tpl_model->msg($this->_tpl_model->user_msg(), 'danger');
					}
				}
			}
		}
		$this->_tpl_model->show();
	}
	function logout()
	{
		$this->_tpl_model->user_logout(1);
		redirect($this->_tpl_model->_url.$this->_tpl_model->config('dashboard', 'login_uri'));
	}
}
