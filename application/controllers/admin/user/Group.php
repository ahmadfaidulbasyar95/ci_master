<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function index()
	{
		echo $this->_tpl_model->button('admin/user/group/form', 'Add Group', 'fa fa-plus', 'modal_reload', 'style="margin-bottom: 15px;"', 1);
		echo $this->_tpl_model->button('admin/user/group/field?return='.urlencode($this->_tpl_model->_url_current), 'Global Field', 'fa fa-pencil', '', 'style="margin-bottom: 15px;margin-left: 15px;"', 0);

		$form = $this->_pea_model->newForm('user_group');
	
		$form->initRoll('WHERE 1 ORDER BY `id` ASC');

		$form->roll->addInput('title', 'sqllinks');
		$form->roll->input->title->setTitle('Title');
		$form->roll->input->title->setLinks('admin/user/group/form');
		$form->roll->input->title->setModal();
		$form->roll->input->title->setModalReload();
		$form->roll->input->title->setDisplayColumn();

		$form->roll->addInput('type', 'select');
		$form->roll->input->type->setTitle('Type');
		$form->roll->input->type->addOptions($this->_tpl_model->user_group_type);
		$form->roll->input->type->setPlainText();
		$form->roll->input->type->setDisplayColumn();

		$form->roll->addInput('menu_ids', 'multiselect');
		$form->roll->input->menu_ids->setTitle('Menu');
		$form->roll->input->menu_ids->setReferenceTable('menu');
		$form->roll->input->menu_ids->setReferenceField('title','id');
		$form->roll->input->menu_ids->setReferenceCondition('`protect`=1');
		$form->roll->input->menu_ids->setReferenceNested('par_id');
		$form->roll->input->menu_ids->setReferenceOrderBy('orderby');
		$form->roll->input->menu_ids->addAttr('size="10"');
		$form->roll->input->menu_ids->addOption('All Menu', 'all');
		$form->roll->input->menu_ids->setDelimiter('<br>');
		$form->roll->input->menu_ids->setDelimiterAlt(' , ');
		$form->roll->input->menu_ids->setPlainText();
		$form->roll->input->menu_ids->setDisplayColumn();

		$form->roll->addInput('approval', 'select');
		$form->roll->input->approval->setTitle('Registration Approval');
		$form->roll->input->approval->addOption('Registration Closed', '0');
		$form->roll->input->approval->addOption('Direct Approve', '1');
		$form->roll->input->approval->addOption('Manual Approve', '2');
		$form->roll->input->approval->setPlainText();
		$form->roll->input->approval->setDisplayColumn();
		$form->roll->input->approval->setDisplayFunction(function($value='', $id=0, $index='', $values=array())
		{
			if ($values[$index]['approval']) {
				$value .= ' - <a href="'.$this->_tpl_model->_url.'user/data/register?id='.$id.'" target="_BLANK" title="Goto register form"><i class="far fa-share-square"></i></a>';
			}
			return $value;
		});

		$form->roll->addInput('global_field', 'checkbox');
		$form->roll->input->global_field->setTitle('Global Field');
		$form->roll->input->global_field->setCaption('Yes');
		$form->roll->input->global_field->setPlainText();
		$form->roll->input->global_field->setDisplayColumn();

		$form->roll->addInput('field', 'editlinks');
		$form->roll->input->field->setTitle('Custom Field');
		$form->roll->input->field->setCaption('Manage');
		$form->roll->input->field->setLinks('admin/user/group/field');
		$form->roll->input->field->setDisplayColumn();

		$form->roll->addInput('created', 'datetime');
		$form->roll->input->created->setTitle('Created');
		$form->roll->input->created->setPlainText();
		$form->roll->input->created->setDisplayColumn();

		$form->roll->addInput('updated', 'datetime');
		$form->roll->input->updated->setTitle('Updated');
		$form->roll->input->updated->setPlainText();
		$form->roll->input->updated->setDisplayColumn(false);

		$form->roll->onDelete(function($id, $f)
		{
			$f->db->delete('user_field', '`group_id`='.$id);
		});
		
		$form->roll->setDeleteCondition('{roll_id}==1');
		$form->roll->setSaveTool(false);
		$form->roll->action();
		echo $form->roll->getForm();
		$this->_tpl_model->show();
	}
	function form()
	{
		$id   = @intval($_GET['id']);
		$form = $this->_pea_model->newForm('user_group');

		$_GET['return'] = '';
		$this->_tpl_model->setLayout('blank');
		
		$form->initEdit(!empty($id) ? 'WHERE `id`='.$id : '');

		$form->edit->setHeader(!empty($id) ? 'Edit Group' : 'Add Group');
		$form->edit->setModalResponsive();

		$form->edit->addInput('title', 'text');
		$form->edit->input->title->setTitle('Title');
		$form->edit->input->title->setRequire();

		$form->edit->addInput('type', 'select');
		$form->edit->input->type->setTitle('Type');
		$form->edit->input->type->addOptions($this->_tpl_model->user_group_type);

		$form->edit->addInput('menu_ids', 'multiselect');
		$form->edit->input->menu_ids->setTitle('Menu');
		$form->edit->input->menu_ids->setReferenceTable('menu');
		$form->edit->input->menu_ids->setReferenceField('title','id');
		$form->edit->input->menu_ids->setReferenceCondition('`protect`=1');
		$form->edit->input->menu_ids->setReferenceNested('par_id');
		$form->edit->input->menu_ids->setReferenceOrderBy('orderby');
		$form->edit->input->menu_ids->setDependent($form->edit->input->type->getName(), 'type');
		$form->edit->input->menu_ids->addAttr('size="10"');
		$form->edit->input->menu_ids->addOption('All Menu', 'all');

		$form->edit->addInput('approval', 'select');
		$form->edit->input->approval->setTitle('Registration Approval');
		$form->edit->input->approval->addOption('Registration Closed', '0');
		$form->edit->input->approval->addOption('Direct Approve', '1');
		$form->edit->input->approval->addOption('Manual Approve', '2');

		$form->edit->addInput('global_field', 'checkbox');
		$form->edit->input->global_field->setTitle('Include Global Field');
		$form->edit->input->global_field->setCaption('Yes');

		if ($id == 1) {
			$form->edit->input->type->setPlainText();
			$form->edit->input->menu_ids->setDelimiter('<br>');
			$form->edit->input->menu_ids->setPlainText();
			$form->edit->input->approval->setPlainText();
		}
		
		$form->edit->action();
		echo $form->edit->getForm();
		$this->_tpl_model->show();
	}

	function field()
	{
		$id = @intval($_GET['id']);
		if ($id) {
			$group_title = $this->_tpl_model->_db_model->getOne('SELECT `title` FROM `user_group` WHERE `id`='.$id);
			if ($group_title) {
				$this->_tpl_model->nav_add('admin/user/group/field?id='.$id, 'Custom Field');
				$this->_tpl_model->nav_add($group_title);
			}
		}else{
			$this->_tpl_model->nav_add('Global Field');
		}
		echo $this->_tpl_model->button('admin/user/group/field_form?group_id='.$id, 'Add Field', 'fa fa-plus', 'modal_reload modal_large', 'style="margin-bottom: 15px;"', 1);

		$form = $this->_pea_model->newForm('user_field');
	
		$form->initRoll('WHERE `group_id`='.$id.' ORDER BY `orderby` ASC');

		$form->roll->addInput('name', 'sqllinks');
		$form->roll->input->name->setTitle('Name');
		$form->roll->input->name->setLinks('admin/user/group/field_form');
		$form->roll->input->name->setModal();
		$form->roll->input->name->setModalReload();
		$form->roll->input->name->setModalLarge();
		$form->roll->input->name->setDisplayColumn();

		$form->roll->addInput('title', 'sqlplaintext');
		$form->roll->input->title->setTitle('Title');
		$form->roll->input->title->setDisplayColumn();

		$form->roll->addInput('form', 'sqlplaintext');
		$form->roll->input->form->setTitle('Form Type');
		$form->roll->input->form->setDisplayColumn();

		$form->roll->addInput('required', 'checkbox');
		$form->roll->input->required->setTitle('Required');
		$form->roll->input->required->setCaption('yes');
		$form->roll->input->required->setPlaintext();
		$form->roll->input->required->setDisplayColumn();

		$form->roll->addInput('params', 'sqlplaintext');
		$form->roll->input->params->setTitle('Parameter');
		$form->roll->input->params->setDisplayColumn();
		$form->roll->input->params->setDisplayFunction(function($value='')
		{
			$value = @(array)json_decode($value, 1);
			$out   = array();
			$out2  = array();
			foreach ($value as $key1 => $value1) {
				$out[$value1['method']] = @intval($out[$value1['method']]) + 1;
			}
			foreach ($out as $key1 => $value1) {
				if ($value1 != 1) {
					$key1 .= '('.$value1.')';
				}
				$out2[] = $key1;
			}
			return implode(', ', $out2);
		});

		$form->roll->addInput('orderby', 'orderby');
		$form->roll->input->orderby->setTitle('Ordered');
		$form->roll->input->orderby->setDisplayColumn();
		if (isset($_GET[$form->roll->sortConfig['get_name']])) {
			$form->roll->input->orderby->setPlaintext();
		}

		$form->roll->addInput('created', 'datetime');
		$form->roll->input->created->setTitle('Created');
		$form->roll->input->created->setPlainText();
		$form->roll->input->created->setDisplayColumn();

		$form->roll->addInput('updated', 'datetime');
		$form->roll->input->updated->setTitle('Updated');
		$form->roll->input->updated->setPlainText();
		$form->roll->input->updated->setDisplayColumn(false);
		
		$form->roll->action();
		echo $form->roll->getForm();
		$this->_tpl_model->show();
	}
	function field_form()
	{
		$id       = @intval($_GET['id']);
		$group_id = @intval($_GET['group_id']);
		$form     = $this->_pea_model->newForm('user_field');

		$_GET['return'] = '';
		$this->_tpl_model->setLayout('blank');
		
		$form->initEdit(!empty($id) ? 'WHERE `id`='.$id : '');

		if (!$id) {
			$group_title = $this->_tpl_model->_db_model->getOne('SELECT `title` FROM `user_group` WHERE `id`='.$group_id);
			if ($group_title) {
				$form->edit->addExtraField('group_id', $group_id);
			}
		}

		$form->edit->setHeader(!empty($id) ? 'Edit Field' : 'Add Field');
		$form->edit->setModalResponsive();
		$form->edit->bodyWrap($form->edit->formBodyBefore.'<div class="row">','</div>'.$form->edit->formBodyAfter);

		$form->edit->addInput('name', 'text');
		$form->edit->input->name->setTitle('Name');
		$form->edit->input->name->setRequire();
		$form->edit->input->name->formWrap('<div class="col-xs-12 col-sm-6">','</div>');
		$name = $form->edit->input->name->getName();
		if (!empty($_POST[$name])) {
			$_POST[$name] = preg_replace('~[^a-z0-9]~', '_', strtolower($_POST[$name]));
		}

		$form->edit->addInput('title', 'text');
		$form->edit->input->title->setTitle('Title');
		$form->edit->input->title->setRequire();
		$form->edit->input->title->formWrap('<div class="col-xs-12 col-sm-6">','</div>');

		$form_type = ['text','checkbox','date','datetime','file','select','multiselect','textarea','hidden'];
		$form->edit->addInput('form', 'select');
		$form->edit->input->form->setTitle('Form Type');
		$form->edit->input->form->setRequire();
		$form->edit->input->form->addAttr('id="s_form"');
		$form->edit->input->form->formWrap('<div class="col-xs-12 col-sm-6">','</div>');
		foreach ($form_type as $value) {
			$form->edit->input->form->addOption($value, $value);
		}

		$form->edit->addInput('required', 'checkbox');
		$form->edit->input->required->setTitle('Required');
		$form->edit->input->required->setCaption('yes');
		$form->edit->input->required->formWrap('<div class="col-xs-12 col-sm-6">','</div>');

		$form->edit->addInput('params', 'textarea');
		$form->edit->input->params->setTitle('Parameter');
		$form->edit->input->params->addAttr('id="s_params"');		
		$form->edit->input->params->addTip('<div id="s_params_result">
			<div class="form-inline" style="margin-bottom: 10px;">
				<div class="form-group">
					<select name="params[{index}][method]" class="form-control s_method">
						<option value="">-- Select Method --</option>
					</select>
				</div>
				<div class="form-group s_args" data-index="{index}">
					<input type="text" name="params[{index}][args][]" class="form-control i_text" title="{data}" placeholder="{data}" value="{value}" size="40">
					<input type="number" name="params[{index}][args][]" class="form-control i_number" title="{data}" placeholder="{data}" value="{value}">
					<select name="params[{index}][args][]" class="form-control i_select">
						{data}
					</select>
				</div>
				<div class="form-group">
					<a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
				</div>
			</div>
		</div>
		<a id="s_params_add" href="#" class="btn btn-default"><i class="fa fa-plus"></i></a>');		
		$form->edit->input->params->formWrap('<div class="col-xs-12 col-sm-12">','</div>');		
		$params = $form->edit->input->params->getName();
		if (isset($_POST[$params])) {
			$params_allow   = array('setCaption','setType','addTip','addClass','addAttr','setFormat','setDefaultValue','setPlainTextCondition','setDateFormat','setDateFormatInput','setMinDate','setMaxDate','setFolder','setAllowedExtension','setResize','setThumbnail','setImageClick','setDocumentViewer','addOption','setHtmlEditor','setNameEncode','setUrlExpire');
			$_POST[$params] = (isset($_POST['params'])) ? (array)$_POST['params'] : array();
			foreach ($_POST[$params] as $key => $value) {
				if (empty($value['method'])) {
					unset($_POST[$params][$key]);
				}elseif (!in_array($value['method'], $params_allow)) {
					unset($_POST[$params][$key]);
				}elseif (empty($value['args'])) {
					$_POST[$params][$key]['args'] = array();
				}
			}
			$_POST[$params] = json_encode(array_values($_POST[$params]));
		}

		$this->_tpl_model->js('controllers/admin/user/group_field_form.js');

		$form->edit->action();
		echo $form->edit->getForm();
		$this->_tpl_model->show();
	}
}
