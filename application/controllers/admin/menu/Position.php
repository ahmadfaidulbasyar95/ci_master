<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function index()
	{
		$form = $this->_pea_model->newForm('menu_position');

		$form->initEdit();

		$form->edit->setHeader('Add Menu Position');

		$form->edit->addInput('title', 'text');
		$form->edit->input->title->setTitle('Title');
		$form->edit->input->title->setRequire();
		
		$form->edit->action();
	
		$form->initRoll('WHERE 1 ORDER BY `id` ASC');

		$form->roll->setHeader('Menu Position');

		$form->roll->addInput('id', 'sqlplaintext');
		$form->roll->input->id->setTitle('ID');
		$form->roll->input->id->setDisplayColumn();

		$form->roll->addInput('title', 'text');
		$form->roll->input->title->setTitle('Title');
		$form->roll->input->title->setRequire();

		$form->roll->addInput('created', 'datetime');
		$form->roll->input->created->setTitle('Created');
		$form->roll->input->created->setPlainText();
		$form->roll->input->created->setDisplayColumn();

		$form->roll->addInput('updated', 'datetime');
		$form->roll->input->updated->setTitle('Updated');
		$form->roll->input->updated->setPlainText();
		$form->roll->input->updated->setDisplayColumn(false);
		
		$form->roll->onDelete(function($id, $f)
		{
			$f->db->exec('DELETE FROM `menu` WHERE `position_id`='.$id);
		});
		$form->roll->action();
		echo $form->roll->getForm();
		echo $this->_tpl_model->msg('Deleting this data will also deleting Menu in Position','warning');
		
		echo $form->edit->getForm();

		if ($_POST) {
			$this->_tpl_model->clean_cache('files/cache_sys/menu');
			$this->_tpl_model->clean_cacheSession('cache_menu');
		}
		
		$this->_tpl_model->show();
	}
}
