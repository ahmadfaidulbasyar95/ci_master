<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Credentials extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function telegram()
	{
		$form = $this->_pea_model->newForm('config');

		$form->initEdit('WHERE `name`="telegram"', 'name', 1);		
		$form->edit->setHeader('Telegram Configuration');

		$form->edit->addInput('value', 'params');
		$form->edit->input->value->setTitle('');

		$form->edit->input->value->addInput('active', 'checkbox');
		$form->edit->input->value->element->active->setTitle('Enable');
		$form->edit->input->value->element->active->setCaption('Yes');
		$form->edit->input->value->element->active->addAttr('id="active"');

		$form->edit->input->value->addInput('token', 'text');
		$form->edit->input->value->element->token->setTitle('Bot Token');
		$form->edit->input->value->element->token->addAttr('id="token"');

		$form->edit->input->value->addInput('data', 'hidden');
		$form->edit->input->value->element->data->addAttr('id="data"');

		$form->edit->action();
		echo $form->edit->getForm().'<div id="info"></div>';
		$this->_tpl_model->js('controllers/admin/config/credentials_telegram.js');

		if ($_POST) {
			$this->_tpl_model->clean_cacheSession('cache_cfg');
		}
		
		$this->_tpl_model->show();
	}

	function google()
	{
		$form = $this->_pea_model->newForm('config');

		$form->initEdit('WHERE `name`="google"', 'name', 1);		
		$form->edit->setHeader('Google Configuration');

		$form->edit->addInput('value', 'params');
		$form->edit->input->value->setTitle('');

		$form->edit->input->value->addInput('client_id', 'text');
		$form->edit->input->value->element->client_id->setTitle('Client ID');
		$form->edit->input->value->element->client_id->setRequire();

		$form->edit->action();
		echo $form->edit->getForm();

		if ($_POST) {
			$this->_tpl_model->clean_cacheSession('cache_cfg');
		}
		
		$this->_tpl_model->show();
	}

	function email()
	{
		$form = $this->_pea_model->newForm('config');

		$form->initEdit('WHERE `name`="email"', 'name', 1);		
		$form->edit->setHeader('E-Mail Configuration');

		$form->edit->addInput('value', 'params');
		$form->edit->input->value->setTitle('');

		$form->edit->input->value->addInput('from_name', 'text');
		$form->edit->input->value->element->from_name->setTitle('Default From (Name)');
		$form->edit->input->value->element->from_name->setRequire();

		$form->edit->input->value->addInput('from_email', 'text');
		$form->edit->input->value->element->from_email->setTitle('Default From (E-Mail)');
		$form->edit->input->value->element->from_email->setType('email');
		$form->edit->input->value->element->from_email->setRequire();

		$form->edit->input->value->addInput('protocol', 'select');
		$form->edit->input->value->element->protocol->setTitle('Protocol');
		$form->edit->input->value->element->protocol->addOption('Mail','mail');
		$form->edit->input->value->element->protocol->addOption('Sendmail','sendmail');
		$form->edit->input->value->element->protocol->addOption('SMTP','smtp');

		$form->edit->input->value->addInput('smtp_host', 'text');
		$form->edit->input->value->element->smtp_host->setTitle('SMTP Server Address');

		$form->edit->input->value->addInput('smtp_user', 'text');
		$form->edit->input->value->element->smtp_user->setTitle('SMTP Username');

		$form->edit->input->value->addInput('smtp_pass', 'text');
		$form->edit->input->value->element->smtp_pass->setTitle('SMTP Password');
		$form->edit->input->value->element->smtp_pass->setType('password');

		$form->edit->input->value->addInput('smtp_port', 'text');
		$form->edit->input->value->element->smtp_port->setTitle('SMTP Port');

		$form->edit->input->value->addInput('smtp_timeout', 'text');
		$form->edit->input->value->element->smtp_timeout->setTitle('SMTP Timeout (in seconds)');

		$form->edit->input->value->addInput('smtp_crypto', 'select');
		$form->edit->input->value->element->smtp_crypto->setTitle('SMTP Encryption');
		$form->edit->input->value->element->smtp_crypto->addOption('None','');
		$form->edit->input->value->element->smtp_crypto->addOption('TLS','tls');
		$form->edit->input->value->element->smtp_crypto->addOption('SSL','ssl');

		$form->edit->action();
		echo $form->edit->getForm();

		if ($_POST) {
			$this->_tpl_model->clean_cacheSession('cache_cfg');
		}
		
		$this->_tpl_model->show();
	}

}
