<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function index()
	{
		echo $this->_tpl_model->button('admin/config/email/form', 'Add E-Mail Template', 'fa fa-plus', 'modal_reload modal_large', 'style="margin-bottom: 15px;"', 1);

		$form = $this->_pea_model->newForm('config_email');
	
		$form->initRoll('WHERE 1 ORDER BY `id` DESC');

		$form->roll->addInput('name', 'sqllinks');
		$form->roll->input->name->setTitle('Name');
		$form->roll->input->name->setLinks('admin/config/email/form');
		$form->roll->input->name->setModal();
		$form->roll->input->name->setModalReload();
		$form->roll->input->name->setModalLarge();

		$form->roll->addInput('from', 'sqlplaintext');
		$form->roll->input->from->setTitle('From');
		$form->roll->input->from->setFieldName('CONCAT(`from_name`,"|",`from_email`)');
		$form->roll->input->from->setDisplayFunction(function($value='')
		{
			$value = explode('|', $value);

			if (empty($value[0])) $value[0] = 'Default';
			if (empty($value[1])) $value[1] = 'Default';
			
			return $value[0].' ('.$value[1].')';
		});

		$form->roll->addInput('mailtype', 'select');
		$form->roll->input->mailtype->setTitle('Type');
		$form->roll->input->mailtype->addOption('Plain Text',1);
		$form->roll->input->mailtype->addOption('HTML',2);
		$form->roll->input->mailtype->setPlainText();

		$form->roll->addInput('subject', 'sqlplaintext');
		$form->roll->input->subject->setTitle('Subject');

		$form->roll->addInput('created', 'datetime');
		$form->roll->input->created->setTitle('Created');
		$form->roll->input->created->setPlainText();
		$form->roll->input->created->setDisplayColumn();

		$form->roll->addInput('updated', 'datetime');
		$form->roll->input->updated->setTitle('Updated');
		$form->roll->input->updated->setPlainText();
		$form->roll->input->updated->setDisplayColumn(false);

		$form->roll->setSaveTool(false);
		$form->roll->action();
		echo $form->roll->getForm();

		if ($_POST) {
			$tpls = $this->_db_model->getAssoc('SELECT `name`,`from_name`,`from_email`,`mailtype`,`subject`,`message` FROM `config_email`');
			lib_file_write($this->_pea_model->_root.'files/cache_sys/config_email_template.cfg', json_encode($tpls));
		}

		$this->_tpl_model->show();
	}
	
	function form()
	{
		$id   = @intval($_GET['id']);
		$form = $this->_pea_model->newForm('config_email');

		$_GET['return'] = '';
		$this->_tpl_model->setLayout('blank');
		
		$form->initEdit(!empty($id) ? 'WHERE `id`='.$id : '');

		$form->edit->setHeader(!empty($id) ? 'Edit E-Mail Template' : 'Add E-Mail Template');
		$form->edit->setModalResponsive();

		$form->edit->addInput('name', 'text');
		$form->edit->input->name->setTitle('Name');
		$form->edit->input->name->setUniq();
		$form->edit->input->name->setRequire();
		$name = $form->edit->input->name->getName();
		if (!empty($_POST[$name])) {
			$_POST[$name] = preg_replace('~[^a-z0-9]~', '_', strtolower($_POST[$name]));
		}

		$form->edit->addInput('from_name', 'text');
		$form->edit->input->from_name->setTitle('From (Name)');
		$form->edit->input->from_name->addTip('Keep empty to use default setting');

		$form->edit->addInput('from_email', 'text');
		$form->edit->input->from_email->setTitle('From (E-Mail)');
		$form->edit->input->from_email->setType('email');
		$form->edit->input->from_email->addTip('Keep empty to use default setting');

		$form->edit->addInput('mailtype', 'select');
		$form->edit->input->mailtype->setTitle('Type');
		$form->edit->input->mailtype->addOption('Plain Text',1);
		$form->edit->input->mailtype->addOption('HTML',2);
		$form->edit->input->mailtype->addAttr('id="s_mailtype"');

		$form->edit->addInput('subject', 'text');
		$form->edit->input->subject->setTitle('Subject');
		$form->edit->input->subject->setRequire();

		$form->edit->addInput('message', 'textarea');
		$form->edit->input->message->setTitle('Message');
		$form->edit->input->message->setRequire();
		$form->edit->input->message->setHtmlEditor();
		$form->edit->input->message->addAttr('rel="s_message" rows="20"');

		$mailtype = $form->edit->input->mailtype->getName();
		$message  = $form->edit->input->message->getName();
		if (!empty($_POST[$mailtype])) {
			if ($_POST[$mailtype] == 1) {
				$_POST[$message] = $_POST['__'.$message];
			}
		}

		$form->edit->action();
		$this->_tpl_model->js('controllers/admin/config/email_form.js');
		echo $form->edit->getForm();

		if ($_POST) {
			$tpls = $this->_db_model->getAssoc('SELECT `name`,`from_name`,`from_email`,`mailtype`,`subject`,`message` FROM `config_email`');
			lib_file_write($this->_pea_model->_root.'files/cache_sys/config_email_template.cfg', json_encode($tpls));
		}

		$this->_tpl_model->show();
	}
}
