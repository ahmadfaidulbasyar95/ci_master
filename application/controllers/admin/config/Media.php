<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function index()
	{
		echo $this->_tpl_model->button('admin/config/media/form', 'Add Media Template', 'fa fa-plus', 'modal_reload modal_large', 'style="margin-bottom: 15px;"', 1);

		$form = $this->_pea_model->newForm('config_media');
	
		$form->initRoll('WHERE 1 ORDER BY `id` DESC');

		$form->roll->addInput('name', 'sqllinks');
		$form->roll->input->name->setTitle('Name');
		$form->roll->input->name->setLinks('admin/config/media/form');
		$form->roll->input->name->setModal();
		$form->roll->input->name->setModalReload();
		$form->roll->input->name->setModalLarge();

		$form->roll->addInput('created', 'datetime');
		$form->roll->input->created->setTitle('Created');
		$form->roll->input->created->setPlainText();
		$form->roll->input->created->setDisplayColumn();

		$form->roll->addInput('updated', 'datetime');
		$form->roll->input->updated->setTitle('Updated');
		$form->roll->input->updated->setPlainText();
		$form->roll->input->updated->setDisplayColumn(false);

		$form->roll->setSaveTool(false);
		$form->roll->action();
		echo $form->roll->getForm();

		if ($_POST) {
			$tpls = $this->_db_model->getAssoc('SELECT `name`,`message` FROM `config_media`');
			lib_file_write($this->_pea_model->_root.'files/cache_sys/config_media_template.cfg', json_encode($tpls));
		}

		$this->_tpl_model->show();
	}
	function form()
	{
		$id   = @intval($_GET['id']);
		$form = $this->_pea_model->newForm('config_media');

		$_GET['return'] = '';
		$this->_tpl_model->setLayout('blank');
		
		$form->initEdit(!empty($id) ? 'WHERE `id`='.$id : '');

		$form->edit->setHeader(!empty($id) ? 'Edit Media Template' : 'Add Media Template');
		$form->edit->setModalResponsive();

		$form->edit->addInput('name', 'text');
		$form->edit->input->name->setTitle('Name');
		$form->edit->input->name->setUniq();
		$form->edit->input->name->setRequire();
		$name = $form->edit->input->name->getName();
		if (!empty($_POST[$name])) {
			$_POST[$name] = preg_replace('~[^a-z0-9]~', '_', strtolower($_POST[$name]));
		}

		$form->edit->addInput('message', 'textarea');
		$form->edit->input->message->setTitle('Message');
		$form->edit->input->message->setRequire();
		$form->edit->input->message->addAttr('rows="20"');

		$form->edit->action();
		echo $form->edit->getForm();

		if ($_POST) {
			$tpls = $this->_db_model->getAssoc('SELECT `name`,`message` FROM `config_media`');
			lib_file_write($this->_pea_model->_root.'files/cache_sys/config_media_template.cfg', json_encode($tpls));
		}

		$this->_tpl_model->show();
	}

}
