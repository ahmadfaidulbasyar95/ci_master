<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function index()
	{
		$type = @intval($_GET['type']);
		$form = $this->_pea_model->newForm('content');

		$form->initSearch();
		
		$form->search->addInput('keyword','keyword');
		$form->search->input->keyword->setTitle('Search');
		$form->search->input->keyword->addSearchField('title,intro');

		$form->search->addInput('cat_ids', 'selecttable');
		$form->search->input->cat_ids->setTitle('Category');
		$form->search->input->cat_ids->setReferenceTable('content_cat');
		$form->search->input->cat_ids->setReferenceField( 'title', 'id' );
		$form->search->input->cat_ids->setReferenceCondition( '`type`='.$type );
		$form->search->input->cat_ids->setReferenceNested( 'par_id' );
		$form->search->input->cat_ids->addOption( '-- Select Category --' );
		$form->search->input->cat_ids->setSearchFunction(function($value='') {
			return '`cat_ids` LIKE "%\"'.$value.'\"%"';
		});
		
		$add_sql = $form->search->action();
		$keyword = $form->search->keyword();

		echo $form->search->getForm();

		echo $this->_tpl_model->button('admin/content/data/form', 'Add Content', 'fa fa-plus', 'modal_reload modal_large', 'style="margin-right: 10px; margin-bottom: 15px;"', 1);
		echo $this->_tpl_model->button('admin/content/category', 'Edit Category', 'fa fa-pencil', 'modal_reload modal_large', 'style="margin-right: 10px; margin-bottom: 15px;"', 1);

		$form->initRoll($add_sql.' AND `type`='.$type.' ORDER BY `id` DESC');
				
		$form->roll->addInput('id', 'sqlplaintext');
		$form->roll->input->id->setTitle('ID');
		$form->roll->input->id->setDisplayColumn(0);

		$form->roll->addInput('title_w', 'multiinput');
		$form->roll->input->title_w->setTitle('Title');

		$form->roll->input->title_w->addInput('title', 'sqllinks');
		$form->roll->input->title_w->element->title->setTitle('Title');
		$form->roll->input->title_w->element->title->setLinks('admin/content/data/form?type='.$type);
		
		$form->roll->addInput('image', 'file');
		$form->roll->input->image->setTitle('Image');
		$form->roll->input->image->setFolder('files/content/');
		$form->roll->input->image->setResize(1920);
		$form->roll->input->image->setImageClick();
		$form->roll->input->image->setThumbnail(480, 'thumb/');
		$form->roll->input->image->setPlainText(true);
		$form->roll->input->image->setDisplayColumn();

		$form->roll->addInput('cat_ids', 'multiselect');
		$form->roll->input->cat_ids->setTitle('Category');
		$form->roll->input->cat_ids->setDelimiter('<br>');
		$form->roll->input->cat_ids->setDelimiterAlt(' , ');
		$form->roll->input->cat_ids->setPlainText();
		$form->roll->input->cat_ids->setDisplayColumn();
		$form->roll->input->cat_ids->addOptions($form->search->input->cat_ids->options);
		
		$form->roll->addInput('active', 'checkbox');
		$form->roll->input->active->setTitle('Active');
		$form->roll->input->active->setCaption('yes');

		$form->roll->addInput('created', 'datetime');
		$form->roll->input->created->setTitle('Created');
		$form->roll->input->created->setPlainText();
		$form->roll->input->created->setDisplayColumn();

		$form->roll->addInput('updated', 'datetime');
		$form->roll->input->updated->setTitle('Updated');
		$form->roll->input->updated->setPlainText();
		$form->roll->input->updated->setDisplayColumn(false);

		$form->roll->action();
		echo $form->roll->getForm();

		$this->_tpl_model->show();
	}

	function form()
	{
		$id     = @intval($_GET['id']);
		$type   = @intval($_GET['type']);
		$form   = $this->_pea_model->newForm('content');

		$form->initEdit(!empty($id) ? 'WHERE `id`='.$id : '');
		
		$form->edit->setHeader(!empty($id) ? 'Edit Content' : 'Add Content');
		
		$form->edit->bodyWrap($form->edit->formBodyBefore.'<div class="row">','</div>'.$form->edit->formBodyAfter);

		$form->edit->addInput('title','text');
		$form->edit->input->title->setTitle('Title');
		$form->edit->input->title->setRequire();
		$form->edit->input->title->formWrap('<div class="col-xs-12 col-sm-8 col-md-9">','');

		$form->edit->addInput('intro', 'textarea');
		$form->edit->input->intro->setTitle('Intro');

		$form->edit->addInput('content', 'textarea');
		$form->edit->input->content->setTitle('Content');
		$form->edit->input->content->setHtmlEditor();
		$form->edit->input->content->setRequire();
		$form->edit->input->content->addAttr('rows="20"');
		$form->edit->input->content->formWrap('','</div>');

		$content = $form->edit->input->content->getName();
		if (isset($_POST[$content])) {
			$intro = $form->edit->input->intro->getName();
			if (empty($_POST[$intro])) {
				$_POST[$intro] = substr(strip_tags($_POST[$content]), 0, 255);
			}
		}

		$form->edit->addInput('image', 'file');
		$form->edit->input->image->setTitle('Image');
		$form->edit->input->image->setFolder('files/content/');
		$form->edit->input->image->setResize(1920);
		$form->edit->input->image->setImageClick();
		$form->edit->input->image->setThumbnail(480, 'thumb/');
		$form->edit->input->image->formWrap('<div class="col-xs-12 col-sm-4 col-md-3">','');

		$form->edit->addInput('cat_ids', 'multiselect');
		$form->edit->input->cat_ids->setTitle('Category');
		$form->edit->input->cat_ids->setReferenceTable('content_cat');
		$form->edit->input->cat_ids->setReferenceField('title','id');
		$form->edit->input->cat_ids->setReferenceCondition('`type`='.$type);
		$form->edit->input->cat_ids->setReferenceNested('par_id');
		$form->edit->input->cat_ids->setReferenceOrderBy('orderby');
		$form->edit->input->cat_ids->addAttr('size="10"');

		$form->edit->addInput('active', 'checkbox');
		$form->edit->input->active->setTitle('Active');
		$form->edit->input->active->setCaption('yes');
		$form->edit->input->active->formWrap('','</div>');

		if (!$id) {
			$form->edit->addExtraField('type', $type);
		}
		
		$form->edit->action();
		echo $form->edit->getForm();

		$this->_tpl_model->show();
	}
}
