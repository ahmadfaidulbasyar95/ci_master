(function() {
	window.addEventListener('load', function() { 
		var el  = $('#content_type');
		var elr = $('#content_type_result');
		var eln = $('#content_type_add');
		var x   = elr.html();
		var val = el.val();
		var idx = 0;
		eln.on('click', function(event) {
			event.preventDefault();
			elr.append(x.replace(/{id}/g, idx).replace(/{title}/g, ''));
			idx++;
		});
		elr.on('click', 'a', function(event) {
			event.preventDefault();
			var elh = $(this).parents('.form-inline');
			elh.slideUp(200);
			setTimeout(function() {
				elh.remove();
			}, 200);
		});
		elr.on('keyup change', '[type="number"]', function(event) {
		$(this).val(parseInt($(this).val()));
		});
		elr.html('');
		if (val) {
			val = JSON.parse(val);
			$.each(val.id, function(index, vl) {
				if (idx <= vl) {
					idx = parseInt(vl)+1;
				}
				elr.append(x.replace(/{id}/g, vl).replace(/{title}/g, val.title[index]));
			});
		}else{
			eln.trigger('click');
		}
	}, false);
})();