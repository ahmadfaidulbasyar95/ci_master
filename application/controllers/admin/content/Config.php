<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function index()
	{
		$form = $this->_pea_model->newForm('config');

		$form->initEdit('WHERE `name`="content"', 'name', 1);		
		$form->edit->setHeader('Content Configuration');

		$form->edit->addInput('value', 'params');
		$form->edit->input->value->setTitle('');

		$form->edit->input->value->addInput('img_def', 'file');
		$form->edit->input->value->element->img_def->setTitle('Default Image');
		$form->edit->input->value->element->img_def->setImageClick();
		$form->edit->input->value->element->img_def->setRequire();

		$form->edit->input->value->addInput('type', 'textarea');
		$form->edit->input->value->element->type->setTitle('Content Type');
		$form->edit->input->value->element->type->addAttr('id="content_type" style="display:none;"');
		$form->edit->input->value->element->type->addTip('<div id="content_type_result">
			<div class="form-inline" style="margin-bottom: 10px;">
				<div class="form-group"">
					<input type="number" min="1" max="999" name="content_type_id[]" class="form-control" title="ID" placeholder="ID" value="{id}" size="10" required="required">
				</div>
				<div class="form-group"">
					<input type="text" name="content_type_title[]" class="form-control" title="Title" placeholder="Title" value="{title}" size="40" required="required">
				</div>
				<div class="form-group">
					<a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
				</div>
			</div>
		</div>
		<a id="content_type_add" href="#" class="btn btn-default"><i class="fa fa-plus"></i></a>');		
		if ($_POST) {
			if (!empty($_POST['content_type_id']) and count(@(array)$_POST['content_type_id']) == count(@(array)$_POST['content_type_title'])) {
				$name                     = $form->edit->input->value->element->type->getName();
				$_POST['content_type_ok'] = [
					'id'    => $_POST['content_type_id'],
					'title' => $_POST['content_type_title'],
					'menu'  => $_POST[$name] ? @(array)json_decode($_POST[$name], 1)['menu'] : [],
				];
				$_POST[$name] = json_encode($_POST['content_type_ok']);
			}
		}

		$form->edit->onSave(function($id, $f)
		{
			if (isset($_POST['content_type_ok'])) {
			}
		});

		$form->edit->action();
		$this->_tpl_model->js('controllers/admin/content/config.js');
		echo $form->edit->getForm();

		if ($_POST) {
			$this->_tpl_model->config_save('content');
			$this->_tpl_model->clean_cacheSession('cache_cfg');
		}
		
		$this->_tpl_model->show();
	}

}
