<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function index()
	{
		$id   = @intval($_GET['id']);
		$type = @intval($_GET['type']);
		$this->_tpl_model->lib('tabs');

		if ($id) {
			$par_id_search = $id;
			$nav_adds      = array();
			while ($par_id_search) {
				$par_data = $this->_tpl_model->_db_model->getRow('SELECT `par_id`,`title` FROM `content_cat` WHERE `id`='.$par_id_search);
				if ($par_data) {
					$nav_adds[]    = array($par_id_search,$par_data['title']);
					$par_id_search = $par_data['par_id'];
				}else{
					$par_id_search = 0;
				}
			}
			foreach (array_reverse($nav_adds) as $value) {
				$this->_tpl_model->nav_add('admin/content/category?type='.$type.'&id='.$value[0], $value[1]);
			}
		}

		$form = $this->_pea_model->newForm('content_cat');
		$form->initSearch();

		$form->search->addInput('keyword', 'text');
		$form->search->input->keyword->setTitle('Search');
				
		$add_sql = $form->search->action();
		$keyword = $form->search->keyword();
		
		echo $form->search->getForm();
		echo '<div class="visible-xs" style="height: 150px;"></div>';
		
		$_GET['keyword'] = @$keyword['keyword'];

		$_GET['id']     = 0;
		$_GET['par_id'] = $id;
		$add            = $this->form();
		
		$_GET['id'] = $id;
		$list       = $this->list();

		if ($id) {
			$edit = $this->form();
			
			echo lib_tabs(array(
				'Edit Category'    => $edit,
				'Sub Category'     => $list,
				'Add Sub Category' => $add,
			));
		}else{
			echo lib_tabs(array(
				'Category'     => $list,
				'Add Category' => $add,
			));
		}

		$this->_tpl_model->show();
	}

	function list()
	{
		$id      = @intval($_GET['id']);
		$type    = @intval($_GET['type']);
		$keyword = @$_GET['keyword'];
		$form    = $this->_pea_model->newForm('content_cat');

		$form->initRoll('WHERE `par_id`='.$id.' AND `type`='.$type.' '.(($keyword) ? 'AND `title` LIKE "%'.addslashes($keyword).'%"' :'').' ORDER BY `orderby` ASC');

		$form->roll->addInput('id', 'sqlplaintext');
		$form->roll->input->id->setTitle('ID');
		$form->roll->input->id->setDisplayColumn(0);

		$form->roll->addInput('title_w', 'multiinput');
		$form->roll->input->title_w->setTitle('Title');

		$form->roll->input->title_w->addInput('title', 'sqllinks');
		$form->roll->input->title_w->element->title->setTitle('Title');
		$form->roll->input->title_w->element->title->setLinks('admin/content/category?type='.$type);

		$form->roll->input->title_w->addInput('url', 'editlinks');
		$form->roll->input->title_w->element->url->setTitle('<i class="fa fa-external-link pull-right" title="Open URL"></i>');
		$form->roll->input->title_w->element->url->setLinks('c');

		$form->roll->addInput('image', 'file');
		$form->roll->input->image->setTitle('Image');
		$form->roll->input->image->setFolder('files/content_cat/');
		$form->roll->input->image->setResize(1920);
		$form->roll->input->image->setImageClick();
		$form->roll->input->image->setThumbnail(480, 'thumb/');
		$form->roll->input->image->setPlainText(true);
		$form->roll->input->image->setDisplayColumn();
		
		$form->roll->addInput('active', 'checkbox');
		$form->roll->input->active->setTitle('Active');
		$form->roll->input->active->setCaption('yes');

		$form->roll->addInput('orderby', 'orderby');
		$form->roll->input->orderby->setTitle('Ordered');
		if (isset($_GET[$form->roll->sortConfig['get_name']])) {
			$form->roll->input->orderby->setPlaintext();
		}

		$form->roll->addInput('created', 'datetime');
		$form->roll->input->created->setTitle('Created');
		$form->roll->input->created->setPlainText();
		$form->roll->input->created->setDisplayColumn();

		$form->roll->addInput('updated', 'datetime');
		$form->roll->input->updated->setTitle('Updated');
		$form->roll->input->updated->setPlainText();
		$form->roll->input->updated->setDisplayColumn(false);

		function content_cat_on_delete($id, $f)
		{
			$child = $f->db->getAll('SELECT `id`,`image` FROM `content_cat` WHERE `par_id`='.$id);
			if ($child) {
				foreach ($child as $value) {
					if (is_file($f->_root.'files/content_cat/'.$value['image'])) {
						unlink($f->_root.'files/content_cat/'.$value['image']);
						if (is_file($f->_root.'files/content_cat/thumb/'.$value['image'])) {
							unlink($f->_root.'files/content_cat/thumb/'.$value['image']);
						}
					}
					content_cat_on_delete($value['id'], $f);
				}
				$f->db->exec('DELETE FROM `content_cat` WHERE `par_id`='.$id);
			}
		}
		$form->roll->onDelete('content_cat_on_delete');
		$form->roll->action();
		return $form->roll->getForm();
	}

	function form()
	{
		$id     = @intval($_GET['id']);
		$type   = @intval($_GET['type']);
		$par_id = @intval($_GET['par_id']);
		$form   = $this->_pea_model->newForm('content_cat');

		$form->initEdit(!empty($id) ? 'WHERE `id`='.$id.' AND `type`='.$type : '');
		
		$form->edit->setHeader(!empty($id) ? 'Edit Category' : 'Add Category');
		
		$form->edit->addInput('title','text');
		$form->edit->input->title->setTitle('Title');
		$form->edit->input->title->setRequire();

		$form->edit->addInput('image', 'file');
		$form->edit->input->image->setTitle('Image');
		$form->edit->input->image->setFolder('files/content_cat/');
		$form->edit->input->image->setResize(1920);
		$form->edit->input->image->setImageClick();
		$form->edit->input->image->setThumbnail(480, 'thumb/');

		$form->edit->addInput('active', 'checkbox');
		$form->edit->input->active->setTitle('Active');
		$form->edit->input->active->setCaption('yes');

		if (!$id) {
			if ($par_id) {
				$form->edit->addExtraField('par_id', $par_id);
				$form->edit->addExtraField('type', $form->db->getOne('SELECT `type` FROM `content_cat` WHERE `id`='.$par_id));
			}else{
				$form->edit->addExtraField('type', $type);
			}
		}
		
		$form->edit->action();
		return $form->edit->getForm();
	}
}
