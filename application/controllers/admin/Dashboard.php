<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('_pea_model');
		$this->load->model('_tpl_model');

		$this->_tpl_model->menu_unprotect('index');
		$this->_tpl_model->menu_unprotect('main');

		$this->_tpl_model->user_login_validate(1);

		$this->_tpl_model->setTemplate('admin');
		$this->_tpl_model->nav_add('admin/dashboard/main', '<i class="fa fa-home"></i>', '0');
	}

	function index()
	{
		$this->_tpl_model->setLayout('blank');
		$this->_tpl_model->view('dashboard/index');
		$this->_tpl_model->show();
	}

	function main()
	{
		$this->_tpl_model->setLayout('blank');
		$this->_tpl_model->view('dashboard/main');
		$this->_tpl_model->show();
	}

	function clean_cache()
	{
		$this->_tpl_model->clean_cache();
		echo $this->_tpl_model->msg('Clean Cache Success');
		$this->_tpl_model->show();
	}

}
