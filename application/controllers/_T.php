<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class _T extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
	}

	public function _remap($task = '', $params = array())
	{
		$path = APPPATH.'libraries/pea/task/'.$task.'.php';
		if (file_exists($path)) {
			include $path;
		}
	}
}
