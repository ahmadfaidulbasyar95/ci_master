<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class _encrypt_model extends CI_Model {

	var $encryption_key = '*#@jh$%[*H@nb]+)@Dhl;,E';

	function __construct() {}

	function decodeToken($string, $key = '')
	{
		$output = '';
		$data   = $this->decode($string, $key);
		$data   = json_decode($data, 1);
		if (@$data['string'] and @$data['expired']) {
			if (time() <= $data['expired']) $output = $data['string'];
		}
		return $output;
	}

	function encodeToken($string, $expired_in = 60, $key = '')
	{
		$data = array(
			'string'  => $string,
			'expired' => strtotime('+ '.$expired_in.' minutes'),
		);
		return $this->encode(json_encode($data), $key);
	}

	function encode($string, $key = '')
	{
		$string = substr(date('c'),0,19).$string;
		$key    = $this->get_key($key);
		$iv     = substr(bin2hex(openssl_random_pseudo_bytes(16)),0,16);
		$output = base64_encode(base64_encode($iv).openssl_encrypt($string, 'AES-256-CBC', $key, 0, $iv));
		return $output;
	}

	function decode($string, $key = '')
	{
		$string = base64_decode($string);
		$key    = $this->get_key($key);
		$output = '';
		if (strlen($string) > 24)
		{
			$iv        = base64_decode(substr($string, 0, 24));
			$decrypted = openssl_decrypt(substr($string, 24), 'AES-256-CBC', $key, 0, $iv);
			$output    = substr($decrypted, 19);
		}
		return $output;
	}

	function get_key($key = '')
	{
		if ($key == '')
		{
			if(_SALT) {
				$key = _SALT;
			}else{
				$key = $this->encryption_key;
			}
		}
		return md5($key);
	}

	function set_key($key = '')
	{
		$this->encryption_key = $key;
	}

}