<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class _db_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getAll($query = '')
	{
		return $this->db->query($query)->result_array();
	}

	public function getAssoc($query = '')
	{
		$r = [];
		$q = $this->db->query($query)->result_array();
		if ($q) {
			$index = array_keys($q[0])[0];
			foreach ($q as $value) {
				$r[$value[$index]] = $value;
			}
		}
		return $r;
	}

	public function getCol($query = '')
	{
		$r = [];
		$q = $this->db->query($query)->result_array();
		if ($q) {
			$index = array_keys($q[0])[0];
			foreach ($q as $value) {
				$r[] = $value[$index];
			}
		}
		return $r;
	}

	public function getRow($query = '')
	{
		$r = [];
		$q = $this->db->query($query)->result_array();
		if ($q) {
			$r = $q[0];
		}
		return $r;
	}

	public function getOne($query = '')
	{
		$r = '';
		$q = $this->db->query($query)->result_array();
		if ($q) {
			$r = array_values($q[0])[0];
		}
		return $r;
	}

	public function cache($method = '', $query = '', $path = 'files/cache/')
	{
		$file = FCPATH.$path.'query/'.preg_replace('/[^a-z0-9_-]+/', '-', strtolower($query)).'.cfg';
		if (is_file($file)) {
			$ret = @(array)json_decode(file_get_contents($file),1);
			return isset($ret['string_data']) ? $ret['string_data'] : $ret;
		}else{
			include_once FCPATH.'application/libraries/file.php';
			$ret = $this->$method($query);
			$chc = is_array($ret) ? $ret : ['string_data' => $ret];
			lib_file_write($file, json_encode($chc));
			return $ret;
		}
	}

	public function cacheSession($method = '', $query = '', $path = 'cache')
	{
		$file = preg_replace('/[^a-z0-9_-]+/', '-', strtolower($query));
		if (isset($_SESSION[$path][$file])) {
			return $_SESSION[$path][$file];
		}else{
			$ret                    = $this->$method($query);
			$_SESSION[$path][$file] = $ret;
			return $ret;
		}
	}

	public function exec($query = '')
	{
		$query = $this->db->simple_query($query);
		return $query;
	}

	public function insert($table = '', $data = array())
	{
		$query = (isset($data[0])) ? $this->db->insert_batch($table, $data): $this->db->insert($table, $data);
		return ($query) ? $this->db->insert_id() : $query;
	}

	public function update($table = '', $data = array(), $where = array())
	{
		if (is_numeric($where)) {
			$where = array('id' => $where);
		}
		$query = $this->db->update($table, $data, $where);
		return $query;
	}

	public function delete($table = '', $where = array())
	{
		if (is_numeric($where)) {
			$where = array('id' => $where);
		}
		$query = $this->db->delete($table, $where);
		return $query;
	}

	public function log()
	{
		return $this->db->queries;
	}

	public function config($name = '', $index = '')
	{
		$ret = ($index) ? '' : array();
		if ($name) {
			$dt = array();
			if (isset($_SESSION['cache_cfg'][$name])) {
				$dt = $_SESSION['cache_cfg'][$name];
			}else{
				$fl = FCPATH.'files/cache_sys/config/'.$name.'.cfg';
				if (is_file($fl)) {
					$dt = @(array)json_decode(file_get_contents($fl), 1);
					if ($dt) {
						$_SESSION['cache_cfg'][$name] = $dt;
					}
				}
			}
			if ($dt) {
				if ($index) {
					$ret = @$dt[$index];
				}else{
					$ret = $dt;
				}
			}
		}
		return $ret;
	}
	public function config_save($name = '')
	{
		$dt = $this->getOne('SELECT `value` FROM `config` WHERE `name`="'.$name.'"');
		if ($dt) {
			lib_file_write(FCPATH.'files/cache_sys/config/'.$name.'.cfg', $dt);
		}
	}
	public function config_direct($name = '', $index = '')
	{
		$ret = ($index) ? '' : array();
		if ($name) {
			$dt = array();
			if (isset($_SESSION['cache_cfg'][$name])) {
				$dt = $_SESSION['cache_cfg'][$name];
			}else{
				$dt = $this->getOne('SELECT `value` FROM `config` WHERE `name`="'.$name.'"');
				$dt = @(array)json_decode($dt, 1);
				if ($dt) {
					$_SESSION['cache_cfg'][$name] = $dt;
				}
			}
			if ($dt) {
				if ($index) {
					$ret = @$dt[$index];
				}else{
					$ret = $dt;
				}
			}
		}
		return $ret;
	}

}
