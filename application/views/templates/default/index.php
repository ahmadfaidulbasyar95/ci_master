<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
		$tpl->meta();
		$tpl->css('../admin/includes/libraries/bootstrap-3.4.1-dist/css/bootstrap.min.css');
		$tpl->css('../admin/includes/libraries/fontawesome-5-pro-master/css/all.css');
		$tpl->css('../admin/includes/libraries/font-awesome-4.7.0/css/font-awesome.min.css');
		$tpl->css('../admin/includes/libraries/jquery-ui-1.12.1/jquery-ui.min.css');
		$tpl->css('../admin/includes/libraries/bootstrap-flat-3.3.4-dist/bootstrap-flat.min.css');
		$tpl->css('includes/css/style_additional.css');

		$tpl->js('../admin/includes/libraries/jquery-3.5.1/jquery.min.js');
		$tpl->js('../admin/includes/libraries/bootstrap-3.4.1-dist/js/bootstrap.min.js');
		$tpl->js('../admin/includes/libraries/jquery-ui-1.12.1/jquery-ui.min.js');
		$tpl->js('includes/js/script_additional.js');
		?>
	</head>
	<body>
		<div class="container">
			<div class="x_header">
				<div>
					<img src="<?php echo $tpl->_url.'files/uploads/'.$tpl->config('site','logo'); ?>" class="img-responsive hidden-xs" alt="Image" style="height: 100px;padding: 5px 0;">
				</div>
				<div class="x_header_title">
					<h3 class="text-success"><?php echo $tpl->config('site', 'meta_title'); ?></h3>
					<div><?php echo $tpl->config('site', 'meta_description'); ?></div>
					<br>
				</div>
			</div>
			<nav class="navbar navbar-default" role="navigation" style="background: #516e1b;border: 0;border-bottom: 4px solid #749e2d;">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<ul id="notif-mobile"></ul>
						<img src="<?php echo $tpl->_url.'files/uploads/'.$tpl->config('site','logo'); ?>" class="img-responsive visible-xs" alt="Image" style="height: 40px;margin: 5px;background: white;">
					</div>
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<?php echo $tpl->menu_show(2); ?>
						<ul class="nav navbar-nav navbar-right">
							<?php 
							if($tpl->user) 
							{
								$tpl->load->model('_notif_model');
								$tpl->_notif_model->load(); 
								?>
								<li id="notif_wrap" class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-fw fa-bell"></i><span class="badge" style="background-color: red;display: none;" id="notif_badge">9+</span></a>
									<ul class="dropdown-menu" id="notif">
										<li>
											<a href="<?php echo $tpl->_url.'user/notif'; ?>" class="text-center"><b>Show All</b></a>
										</li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-fw fa-user-circle"></i> <span class="hidden-xs">Halo, </span><b><?php echo $tpl->user['name']; ?></b> <b class="caret" style="float: right;margin: 7.5px 0 7.5px 5px;"></b></a>
									<ul class="dropdown-menu">
										<li class="hidden-xs">
											<img src="<?php echo $tpl->user['image']; ?>" style="background: white;width: 100%;margin-bottom: 5px;">
										</li>
										<?php echo $tpl->menu_show(1, array(
											'wrap' => '[menu]',
										)); ?>
									</ul>
								</li>
								<?php 
							}else{
								echo $tpl->menu_show(3, array(
									'wrap' => '[menu]',
								));
							}
							?>
						</ul>
					</div>
				</div>
			</nav>
			<div>
				<?php echo $tpl->nav_show(); ?>
			</div>
			<?php echo $tpl->content; ?>
			<div style="border-bottom: 20px solid #35b042;"><?php echo $tpl->config('site', 'footer'); ?></div>
		</div>
		<?php echo $tpl->log; ?>
	</body>
</html>