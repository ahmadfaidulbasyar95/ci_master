<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$tpl->load->model('_notif_model');
$tpl->_notif_model->load(1); 
?>
<div id="_desktop" class="menu_close" style="background-image: url('<?php echo $tpl->_url.'files/uploads/'.$tpl->config('dashboard', 'desktop_background'); ?>');">
	<?php 
	$shortcut = $tpl->_db_model->cacheSession('getAll','SELECT * FROM `menu` WHERE `position_id`=0 AND `shortcut`=1 AND `active`=1 ORDER BY `orderby`','cache_menu');
	echo $tpl->menu_show(array(0 => $shortcut), array(
		'wrap' => '<div id="_menu_shortcut">[menu]</div>',
		'item' => '<a href="[url]" target="_iframe" [attr]><i class="[icon]"></i>[title]</a>',
	));
	?>
	<div id="_iframes">
		<div class="_iframes" data-id="{id}" style="display: none;">
			<div class="_iframe_head">
				<span class="_iframe_title ellipsis">{title}</span>
				<a class="_iframe_refresh" href="#"><i class="fa fa-fw fa-refresh"></i></a>
				<a class="_iframe_minimize" href="#"><i class="fa fa-fw fa-window-minimize"></i></a>
				<a class="_iframe_maximize" href="#"><i class="fa fa-fw fa-window-restore"></i></a>
				<a class="_iframe_close" href="#"><i class="fa fa-fw fa-window-close-o"></i></a>
			</div>
			<iframe class="_iframe_body" src="{url}" allowfullscreen="allowfullscreen" webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>
			<div class="_iframe_body_clicker"></div>
		</div>
	</div>
</div>
<div id="_menu" style="display: none;">
	<div id="_user_info">
		<img src="<?php echo $tpl->user['image']; ?>" style="background: white;">
		<a href="<?php echo $tpl->_url.'admin/user/data/profile'; ?>" data-title="<i class='fa fa-fw fa-user-circle'></i>Profile" target="_iframe" class="menu_close">
			<p class="ellipsis" style="margin: 0;"><?php echo $tpl->user['name']; ?></p>
			<p class="ellipsis" style="margin: 0;font-size: small;"><?php echo $tpl->user_ip().' - '.$tpl->user_device(); ?></p>
		</a>
		<a href="<?php echo $tpl->_url.'admin/user/act/logout'; ?>" onclick="return confirm('Logout ?')"><i class="fa fa-sign-out fa-fw"></i></a>
	</div>
	<?php 
	echo $tpl->menu_show(0, array(
		'wrap'     => '<div id="_menu_list">[menu]</div>',
		'item'     => '<a href="[url]" target="_iframe" class="ellipsis menu_close" [attr]><i class="[icon]"></i>[title]</a>',
		'item_sub' => '<a href="[url]" target="_iframe_sub" class="ellipsis" [attr]><i class="[icon]"></i>[title]</a>
									<div>
										[submenu]
									</div>',
	)); 
	?>
</div>
<div id="_taskbar">
	<a id="_start" href="#"><i class="fas fa-ellipsis-v fa-fw"></i> <span>Start</span></a>
	<div id="_notif" class="dropup menu_close">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-fw fa-bell"></i><span class="badge" style="background-color: red;display: none;" id="notif_badge">9+</span></a>
		<ul class="dropdown-menu" id="notif">
			<li>
				<a href="<?php echo $tpl->_url.'admin/user/notif'; ?>" class="text-center" target="_iframe" data-title="<i class='far fa-fw fa-bell'></i> Notification"><b>Show All</b></a>
			</li>
		</ul>
	</div>
	<div id="_iframe_toggles">
		<a class="_iframe_toggles" href="#" data-id="{id}" style="display: none;">
			<span class="_iframe_toggle_title">{title}</span>
		</a>
	</div>
	<span id="_datetime"></span>
</div>
<script type="text/javascript">
	(function() {
		window.addEventListener('load', function() { 
			var _datetime = '<?php echo time(); ?>';
			setInterval(function() {
				_datetime++;
				var date = new Date(parseInt(_datetime)*1000);
				$('#_datetime').html(date.toLocaleTimeString()+'<br>'+date.toDateString());
			}, 1000);
			function _iframe_resize(el, width = 0, height = 0) {
				var fr = el.find('._iframe_body');
				if (width == 0 || height == 0) {
					fr.css('height', '');
					el.css({
						width : '',
						top : 0,
						left : 0
					}).resizable({
						disabled: true
					}).draggable({
						drag: function( event, ui ) {
							ui.position.left = 0;
							ui.position.top = 0;
						}
					});
				}else{
					fr.css('height', height - 32);
					el.css('width', width).resizable({
						disabled: false,
						resize: function( event, ui ) {
							var width_offside = w_width - ui.position.left - 5;
							if (ui.size.width > width_offside) {
								ui.size.width = width_offside;
							}
							var height_offside = w_height - ui.position.top - 35;
							if (ui.size.height > height_offside) {
								ui.size.height = height_offside;
							}
							fr.css('height', ui.size.height - 32);
							el.data('width', ui.size.width);
							el.data('height', ui.size.height);
						}
					}).draggable({
						drag: function( event, ui ) {
							if(ui.position.left < 0) {
								ui.position.left = 0;
							}
							if(ui.position.top < 0) {
								ui.position.top = 0;
							}
							var left_offside = w_width - el.data('width') - 5;
							if(ui.position.left > left_offside) {
								ui.position.left = left_offside;
							}
							var top_offside = w_height - el.data('height') - 35;
							if(ui.position.top > top_offside) {
								ui.position.top = top_offside;
							}
						}
					});
				}
			}
			var w_width        = $(window).width();
			var w_height       = $(window).height();
			var z_index_last   = 1;
			var maximize_count = 0;
			$('#_start').on('click', function(event) {
				event.preventDefault();
				var s_m = $('#_menu');
				s_m.toggle(300).toggleClass('active');
				if (s_m.hasClass('active')) {
					$('._iframe_active').removeClass('_iframe_active');
					$('._iframe_toggle_active').removeClass('_iframe_toggle_active');
				}
			});
			$('.menu_close').on('click', function(event) {
				$('#_menu.active').toggle(300).toggleClass('active');
			});
			$('body').on('click', '._iframe_maximize', function(event) {
				event.preventDefault();
				$(this).children('.fa').toggleClass('fa-window-maximize').toggleClass('fa-window-restore');
				var el = $(this).parents('._iframes');
				el.toggleClass('_iframe_move');
				if (el.data('width') == undefined) {
					if (w_width < w_height) {
						el.data('width', w_width-5);
					}else{
						el.data('width', w_width/2-2);
						maximize_count++;
						if (maximize_count%2 == 0) {
							el.css('left', w_width/2-3);
						}
					}
				}
				if (el.data('height') == undefined) {
					if (w_width < w_height) {
						el.data('height', w_height/2-18);
						maximize_count++;
						if (maximize_count%2 == 0) {
							el.css('top', w_height/2-18);
						}
					}else{
						el.data('height', w_height-36);
					}
				}
				if (el.hasClass('_iframe_move')) {
					_iframe_resize(el, el.data('width'), el.data('height'));
				}else{
					_iframe_resize(el, 0, 0);
				}
			});
			$('body').on('click', '._iframes', function(event) {
				$('._iframe_active').removeClass('_iframe_active');
				$('._iframe_toggle_active').removeClass('_iframe_toggle_active');
				$(this).css('z-index', z_index_last).addClass('_iframe_active');
				$('._iframe_toggles[data-id="'+$(this).data('id')+'"]').addClass('_iframe_toggle_active');
				z_index_last++;
			});
			$('body').on('click', '._iframe_toggles', function(event) {
				event.preventDefault();
				var el = $('._iframes[data-id="'+$(this).data('id')+'"]');
				if (el.hasClass('_iframe_hide')) {
					el.trigger('click').toggle(300).toggleClass('_iframe_hide');
				}else if (el.hasClass('_iframe_active')) {
					el.toggle(300).toggleClass('_iframe_hide');
				}else{
					el.trigger('click');
				}
			});
			$('body').on('click', '._iframe_minimize', function(event) {
				event.preventDefault();
				var el = $(this).parents('._iframes');
				el.toggle(300).toggleClass('_iframe_hide');
			});
			$('body').on('click', '._iframe_close', function(event) {
				event.preventDefault();
				var el = $(this).parents('._iframes');
				el.fadeOut(300);
				$('._iframe_toggles[data-id="'+el.data('id')+'"]').remove();
				setTimeout(function() {
					el.remove();
				}, 300);
			});
			$('body').on('click', '._iframe_refresh', function(event) {
				event.preventDefault();
				var el = $(this).parents('._iframes').find('._iframe_body');
				el.attr('src', el.contents().get(0).location.href);
			});
			$('#_iframe_toggles').sortable({
				placeholder: "ui-state-highlight"
			});
			$('#_iframe_toggles').disableSelection();
			var _iframe_id           = 0;
			var _iframes             = $('#_iframes');
			var _iframe_toggles      = $('#_iframe_toggles');
			var _iframes_item        = _iframes.html();
			var _iframe_toggles_item = _iframe_toggles.html();
			_iframes.html('');
			_iframe_toggles.html('');
			$('body').on('click', 'a[target="_iframe"]', function(event) {
				event.preventDefault();
				var el = $(this);
				var q  = el.data('confirm');
				if (q) {
					if (!confirm(q)) return false;
				}
				var ix = el.data('_iframe_id');
				if (ix) {
					if ($('._iframes[data-id="'+ix+'"]').length) {
						var ok = 0;
					}else{
						var ok = 1;
					}
				}else{
					var ok = 1;
				}
				if (ok) {
					_iframe_id++;
					el.data('_iframe_id', _iframe_id);
					var x = el.data('title');
					var n = (x) ? x : el.html();
					var h = el.attr('href');
					var i = _iframes_item.toString().replace('{title}', n).replace('{id}', _iframe_id).replace('{url}', h);
					var t = _iframe_toggles_item.toString().replace('{title}', n).replace('{id}', _iframe_id);
					_iframes.prepend(i);
					_iframe_toggles.prepend(t);
					$('._iframes[data-id="'+_iframe_id+'"]').trigger('click').toggle(300);
					$('._iframe_toggles[data-id="'+_iframe_id+'"]').toggle(300);
				}else{
					$('._iframes[data-id="'+ix+'"]').trigger('click');
				}
			});
			$('body').on('click', 'a[target="_iframe_sub"]', function(event) {
				event.preventDefault();
				var el  = $(this);
				var sub = el.next('div');
				if (sub.length) {
					if (el.hasClass('active')) {
						sub.slideUp(300).removeClass('active');
						el.removeClass('active');
					}else{
						sub.slideDown(300).addClass('active');
						el.addClass('active');
					}
				}
			});
			$('a[target="_iframe_sub"]').each(function(index, el) {
				var el  = $(this);
				var sub = el.next('div');
				if (sub.length) {
					if (el.hasClass('active')) {
						sub.addClass('active');
					}else{
						sub.hide();
					}
				}
			});
		}, false);
	})();
</script>
<style type="text/css">
#_taskbar {
	height: 35px;
	width: 100%;
	background: #000000e8;
	position: fixed;
	bottom: 0;
	box-shadow: 0 0 5px black;
	z-index: 99999999999999999999;
}

#_start {
	height: 35px;
	display: inline-block;
	color: white;
	padding-right: 10px;
}
#_start:hover {
	background: #403e3ea6;
}
#_start span {
	border-right: 2px solid;
	padding: 5px 10px 5px 0px;
	border-radius: 50%;
}
#_start i {
	border: 2px solid #337ab7;
	padding: 3px;
	border-radius: 50%;
	width: 25px;
	height: 25px;
	margin: 5px;
	transition: .3s;
}
#_menu.active + #_taskbar > #_start i{
	transform: rotate(90deg);
}

#_datetime {
	float: right;
	padding: 5px;
	color: white;
	font-size: 12px;
	line-height: 12px;
	text-align: center;
}

#_menu {
	background: #151414e6;
	box-shadow: 0 0 5px black;
	position: fixed;
	bottom: 35px;
	width: 300px;
	height: 500px;
	max-width: 100%;
	max-height: 80vh;
	padding: 15px 10px 10px 10px;
	z-index: 99999999999999999999;
}

#_user_info > img {
	width: 40px;
	height: 40px;
	border-radius: 50%;
	display: inline-block;
	position: relative;
	top: -19px;
}
#_user_info > a:nth-child(2) {
	color: white;
	display: inline-block;
	font-size: 20px;
	padding: 0 5px;
	width: calc(100% - 100px);
}
#_user_info > a:nth-child(3) {
	color: white;
	font-size: 20px;
	float: right;
}
#_user_info > a:hover {
	background: #403e3ea6;
}

#_menu_list {
	padding-bottom: 10px;
	height: 420px;
	overflow: auto;
	max-height: calc(80vh - 80px);
}
#_menu_list a {
	color: white;
	display: block;
	padding: 5px;
	font-size: 15px;
}
#_menu_list a > i {
	background: #847a7a;
	padding: 5px 0;
	text-align: center;
	width: 26px;
	height: 26px;
	margin-right: 7px;
	font-size: 17px;
}
#_menu_list a.active ,
#_menu_list a:hover {
	background: #403e3ea6;
}
#_menu_list div {
	margin-left: 15px;
	margin-bottom: 5px;
}
#_menu_list a[target="_iframe_sub"]:after {
	font: normal normal normal 14px/1 FontAwesome;
	text-rendering: auto;
	float: right;
	margin-top: 6px;
	margin-right: 5px;
	content: "\f0d7";
}
#_menu_list a.active[target="_iframe_sub"]:after {
	content: "\f0d8";
}

._iframes {
	position: absolute;
	top: 0;
	box-shadow: 0 0 5px gray;
	width: 100%;
}
._iframe_head {
	background: #333;
	text-align: right;
	color: white;
	padding: 5px;
	border: 2px solid black;
	border-bottom: 0;
}
._iframe_active ._iframe_head {
	background: black;
}
._iframe_body {
	border: 2px solid black;
	display: block;
	width: 100%;
	background: white;
	height: calc(100vh - 67px);
}
._iframe_title {
	float: left;
	width: calc(100% - 100px);
	text-align: left;
}
._iframe_title i {
	margin-right: 5px;
}
._iframe_title br {
	display: none;
}
._iframe_refresh ,
._iframe_minimize ,
._iframe_maximize ,
._iframe_close {
	color: white !important;
}
._iframe_refresh:hover ,
._iframe_minimize:hover ,
._iframe_maximize:hover ,
._iframe_close:hover {
	text-shadow: 0 0 5px;
}
._iframe_body_clicker {
	position: absolute;
	width: 100%;
	height: calc(100% - 32px);
	bottom: 0;
	background: #00000014;
}
._iframe_active ._iframe_body_clicker {
	display: none;
}
._iframe_move.ui-draggable-dragging ._iframe_body_clicker ,
._iframe_move.ui-resizable-resizing ._iframe_body_clicker {
	display: block;
}
._iframe_move.ui-draggable-dragging {
	cursor: grabbing;
}
#_iframe_toggles {
	display: -webkit-inline-box;
	overflow: auto;
	width: calc(100% - 230px);
	white-space: nowrap;
}
#_iframe_toggles .ui-state-highlight {
	width: 200px;
	display: inline-block;
	height: 10px;
}
._iframe_toggles {
	display: inline-block;
	height: 35px;
	border-left: 1px solid #ffffff45;
	border-right: 1px solid #ffffff45;
}
._iframe_toggle_title {
	overflow: hidden;
	white-space: nowrap;
	color: white;
	display: flex;
	width: 200px;
	padding: 7px;
}
._iframe_toggle_title i {
	margin-right: 5px;
	font-size: larger;
}
._iframe_toggle_title i:before {
	vertical-align: middle;
}
._iframe_toggles._iframe_toggle_active ,
._iframe_toggles:hover {
	background: #403e3ea6;
}

#_notif {
	display: inline-block;
}
#_notif #notif {
	background-color: transparent;
	border: 0;
	margin-bottom: 10px;
}
#_notif a {
	background-color: #000000c9;
	color: white;
	margin-bottom: 2px;
	border-radius: 5px;
}
#_notif .active > a {
	background-color: #17222fe6;
}
#_notif a:focus,
#_notif a:hover {
	background-color: #1a2f17e6;
}

#notif_badge {
	position: absolute;
	margin-top: -5px;
	margin-left: -5px;
	border-radius: 10px;
}
#notif {
	max-height: 70vh;
	overflow: auto;
}

@media only screen and (max-width: 767px) {
	#_iframe_toggles .ui-state-highlight ,
	._iframe_toggle_title {
		width: 120px;
	}
	#_datetime {
		display: none;
	}
	#_iframe_toggles {
		width: calc(100% - 120px);
	}
}
</style>