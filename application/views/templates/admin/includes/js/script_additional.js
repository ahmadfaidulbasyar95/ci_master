(function() {
	window.addEventListener('load', function() { 
		$('body').on('keydown', function(event) {
			if (event.originalEvent.key == 'F5') {
				event.preventDefault();
				var x = $('._iframe_refresh', '._iframe_active');
				if (x.length) {
					x.trigger('click');
				}else{
					document.location.href = document.location.href;
				}
			}
		});
	}, false);
})();