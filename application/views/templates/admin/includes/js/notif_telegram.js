(function() {
	window.addEventListener('load', function() { 
		function notif_telegram() {
			$.ajax({
				url: _URL+'_T/notif_telegram',
				type: 'GET',
				dataType: 'html',
				data: {},
			})
			.done(function(out) {
				if (out == 'ok') {
					notif_telegram();
				}else{
					notif_telegram_send();
				}
			});
		} 
		function notif_telegram_send() {
			$.ajax({
				url: _URL+'_T/notif_telegram_send',
				type: 'GET',
				dataType: 'html',
				data: {},
			})
			.done(function(out) {
				if (out == 'ok') {
					notif_telegram_send();
				}
			});
		}
		notif_telegram();
	}, false);
})();