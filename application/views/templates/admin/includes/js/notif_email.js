(function() {
	window.addEventListener('load', function() { 
		function notif_email_send() {
			$.ajax({
				url: _URL+'_T/notif_email_send',
				type: 'GET',
				dataType: 'html',
				data: {},
			})
			.done(function(out) {
				if (out == 'ok') {
					notif_email_send();
				}
			});
		}
		notif_email_send();
	}, false);
})();